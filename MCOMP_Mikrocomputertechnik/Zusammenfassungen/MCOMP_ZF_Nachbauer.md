| Begriff                                                      | Erklärung                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| CPU                                                          | Central Prozessing Unit                                      |
| Programmobjekt                                               | Variable, ist ein/mehreren Speicherobjekten zugeordnet       |
| Speicherobjekt                                               | hat bestimmten Platz & Größe in Speicher                     |
| Compiler                                                     | zerlegt Programmobjekte in Speicherobjekte                   |
| Register                                                     | Speicher in CPU                                              |
| Verarbeitungsbreite                                          | Bitanzahl der in einem elementaren Arbeitsschritt zu verarbeitenden Information |
| Informationsbreite                                           | Bitanzahl des Speicherobjekts                                |
| Von Neumann Architektur                                      | CPU, Systembus, Speicher, Ein-/Ausgabe, Systemhardware, Peripherie |
| Harvard Architektur                                          | Wie Neumann, aber Speicher in Programm- und Datenspeicher getrennt |
| Adress-Multiplex                                             | Anzahl Adress-Bits wird halbiert                             |
| Bus                                                          | Daten-/Energieverbindung zwischen mehreren Komponenten       |
| Bustakt                                                      | $f_{Bus}<f_{CPU}$  (meistens)                                |
| Buszyklus                                                    | Information wird über (mehrere) Bustakt(e) übertragen        |
| asynchroner Parallelbus                                      | zwitlicher Ablauf wird über Handshake-Leitungen gesteuert    |
| Alignment                                                    | Ausrichtung der Basisadresse eines Operanden auf die nächste freie Byteadresse einer Speicherzeile |
| Speicher-Organisation                                        | 256MBit, organisiert 32K*8 (Adressen * Bit pro Sepeicherzelle) |
| Memory-Map                                                   | Anordnung einzelner IC-Speicherbausteine im logischen Adressraum |
| Zugriffszeit                                                 | Zeit:  Adresse anlegen → Daten erhalten                      |
| Zykluszeit                                                   | Zeitdauer zwischen aufeinanderfolgenden Adressen             |
| UART                                                         | (Universal Asynchronous Receiver / Transmitter)              |
| (G)PIO ((General Purpose) / Parallel Input)                  | Digitale I/O Baugruppe mit Pins (für Ein- und Ausgabe nutzbar) |
| ADC (Analog to Digital Converter)                            | Wandelt Analoge Eingangsspannung zu diskretem Spannungs-Wert in diskreter Zeit |
| 1. Programmierte Ein/Ausgabe<br />2. Interruptgesteuerter I/O<br />3. Direct Memory Acess | 1. I/O-Register wird zyklisch von Prozessor-Software abgefragt<br />2. I/O- Baugruppe fordert Senden der Daten an<br />3. direktes Übertragen von I/O-Daten in Arbeitsspeicher |
| I/O Quittierungsverfahren (Handshake)                        | Daten-Übertragung wird über Steuerleitung angefragt und bestätigt (ACK) |
| I/O synchron                                                 | mit gemeinsamem Takt (Prozessor+I/O) können viele Daten übertragen werden |
| I/O asynchron                                                | mit getrenntem Takt können einzelne Zeichen weit transportiert werden |
| Embedded System                                              | erfüllt besimmte Aufgaben, Gegenteil von Standard-PC (erfüllt alle möglichen Aufgaben) |
| Serial Date (SDA)                                            | Datenübertragung I2C                                         |
| Serial Clock (SCL)                                           | Clock I2C                                                    |

| Begriff                      | Erklärung                                                    |
| ---------------------------- | ------------------------------------------------------------ |
| ARM (Advanced RISC Machines) | Entwickelt eigene Prozessor-Architektur, weiter verbreitet als PC-übliche x86-Architektur, führt 16 & 32 Bit Befehle aus |
| Compiler                     | wandelt C- in Maschinen-Code, Front-End: interpretiert, prüft und optimiert Code (prozessorunabhängig), Back-End: erzeugt Prozessorbefehle (prozessorabhängig) |
| Assembler                    | wandelt Maschinen- in binären Code                           |
| Cache Hit                    | Prozessor sucht Inhalt von Speicherbereich → befindet sich in Cache |
| Cache Miss                   | Prozessor sucht Inhalt von Speicherbereich → befindet sich nicht in Cache → Prozessor muss auf normalen Speicher zugreifen |



#### Datentypen

| Variable          | Storage size | Value range                |
| ----------------- | ------------ | -------------------------- |
| Nibble (Halbbyte) |              |                            |
| char              | 1 byte       | -128 to 127                |
| unsigned char     | 1 byte       | 0 to 255                   |
| short             | 2 bytes      | $-2^{15}$ to $2^{15}-1$    |
| unsigned short    | 2 bytes      | $0$ to $2^{16}$            |
| int               | 4 bytes      | $-2^{31}$ to $2^{31}-1$    |
| unsigned int      | 4 bytes      | $0$ to $2^{32}$            |
| long              | 8 bytes      | $-2^{63}$ to $2^{63}-1$    |
| unsigned long     | 8 bytes      | $0$ to $2^{64}$            |
| float             | 4 byte       | ±1.401e-45 bis +3.403e+38  |
| double            | 8 byte       | ±4.941e-324 bis 1.798e+308 |

Wortlänge entspricht Bit-Länge des Prozessors (Prozesser: 32Bit, Halbwort: 16Bit, Wort: 32Bit, Doppelwort: 64Bit)



*Computerhistorie (S. 0-39)*



#### Arbeitsweise eines Computers (S. 73-100)

Mehrphasenarbeitsprinzip:

1. Fetch Instruction (Programmbefehl aus Arbeitsspeicher holen)
2. Decode Instruction (Befehl dekodieren)
3. Fetch Operand(s) (benötigte Daten in CPU laden)
4. Execute Instruction (Befehl ausführen / rechnen)
5. Store Operand Result (Ergebnis in Register/extern speichern)



*Mikrocomputerstruktur (S. 101-159)*

lesen | bearbeiten | speichern



#### Ausgänge

- Takt
- Adressen
- Daten
- Byte-Enables
- Read/Write



#### Adress-Decodierung

<img src="../../Typora.assets/image.7K5PL0.png" alt="image.7K5PL0" style="zoom: 33%;" />



#### Arbeitsspeicher

| Schreib-/Lese-Speicher         | Festwert-Speicher                                            |
| ------------------------------ | ------------------------------------------------------------ |
| RAM (Random Acces Memory)      | ROM (Read-Only Memory)                                       |
| flüchtig                       | nicht flüchtig                                               |
| beliebig beschreib-/lesbar     | fester Inhalt / programmierbar                               |
| SRAM (Static) / DRAM (dynamic) | PROM (programmable) / EPROM (UV-erasable) / EEPROM (electrical erasable) |



#### Programmier- & Testumgebung

Komponenten integrierte Entwicklungsumgebung:

- Software Quellcode → Compiler (Assembler-Code in Objektdatei) → Linker (Speicherabbild mit festen physikalischen Adressen)
- Debugging
- C/C++-Library

| Simulation                                                   | Debug-Monitorsoftware                                        | Debug & Trace Hardware                                       | Lgikanalysator                                               |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| + Algorithmen & logische Funktionen ohne Implementierung testen | + I/O & Echtzeit-verhalten testeng                           | + Programm (Breakpoints analysieren)<br />+ Aufzeichnung von Echtzeitdaten | + komplexe Digitalschaltungen analysieren<br />+ Fehler lokalisieren |
| - Laufzeiten & Hardware-Ereignisse schwer simulierbar        | - nimmt Speicher in Target in Anspruch (evtl. nicht vollständige Software testbar) |                                                              |                                                              |



#### Ein- & Ausgabe

| Steuerregister | Statusregister | Befehlsregister   | Datenregister         |
| -------------- | -------------- | ----------------- | --------------------- |
| Betriebsart    | Zustand        | aktueller Prozess | Speicherung I/O-Daten |



#### GPIO definieren (Header-Datei)

> 4 GPIO-Baugruppen:	PORT**0**, PORT**1**, PORT**2**, PORT**3**
>
> Baugruppen werden über 4 **32**-Bit-Register gesteuert:	IOPIN, IOSET, IODIR, IOCLR

<img src="../../Typora.assets/image.XHX3M0.png" alt="image.XHX3M0" style="zoom: 67%;" />

```c
#ifndef __PORTS_H		// mehrfaches, geschachteltes Inkludieren ermoeglichen
#define __PORTS_H

#include <stdint.h>

typedef volatile struct
{
    int32_t IOPIN;
    int32_t IOSET;
    int32_t IODIR;
    int32_t IOCLR;
} PORT_T;

static PORT_T *const PORT0 = (PORT_T *) 0xE0028000;
static PORT_T *const PORT1 = (PORT_T *) 0xE0028010;
static PORT_T *const PORT2 = (PORT_T *) 0xE0028020;
static PORT_T *const PORT3 = (PORT_T *) 0xE0028030;

#endif // __PORTS_H
```



#### UART

| RxD          | TxD           | CTS                                        | RTS                                          |
| ------------ | ------------- | ------------------------------------------ | -------------------------------------------- |
| Receive      | Transmit      | Clear To Send                              | Request To Send                              |
| Serial Input | Serial Output | (Input) Low active, wenn μC Empfangsbereit | (Output) Low active, wenn UART Senden möchte |

![image.OOB0L0](../../Typora.assets/image.OOB0L0.png)

---



#### ADC

<img src="../../Typora.assets/image-20200614125653252.png" alt="image-20200614125653252" style="zoom: 40%;" />

Absolute Messauflösung:	 $dU=\dfrac{(U_{ref+}-U_{ref-})}{2^n}$  



##### DAC

Messauflösung:	$1\ LSB=(V_{DDA}-V_{SSA})/2^{n_{Bit}}$

Ausgangsspannung:	$Hex\_Value*((V_{ref+}-V_{ref-})/2^{n_{Bit}})+V_{ref-}$

V~DDA~ : obere Referenzspannung, typ. 3,3V				V~SSA~ : untere Referenzspannung, typ. 0V



##### Speicherkonfigurationen

| Memory-Mapped I/O                                            | Isolated I/O                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| I/O Register wird durch Memory-Adressraum angesprochen       | I/O Register wird über separaten Adressraum angesprochen (zusätzliche Leitung M/IO# = 1/0) |
| + keine speziellen Befehle nötig<br />+ keine extra Steuersignale nötig | + einfachere Adresscodierung                                 |

<img src="../../Typora.assets/image-20200615171754499.png" alt="image-20200615171754499" style="zoom:40%;" />



*~~C für Embedded Systems~~*



#### I2C – Bus (274 - 316)

![I2C-Communcation-Protocol](../../Typora.assets/I2C-Communcation-Protocol.jpg)



![image.GXEDM0](../../Typora.assets/image.GXEDM0.png)

![image.68Q3M0](../../Typora.assets/image.68Q3M0.png)

| Bus-Idle        | Start (Repeated Start) | R / W#    | Wait            | ACK                 | Stop               |
| --------------- | ---------------------- | --------- | --------------- | ------------------- | ------------------ |
| SDA + SDC = '1' | SDA=↓  &&  SCL='1'     | '1' / '0' | SCL='0' (Slave) | SDA='0' (Empfänger) | SDA=↑  &&  SCL='1' |

**Kollision**:	Beide senden solange, bis 'L' des einen Masters dominiert. Dann stoppt anderer Master.

| Reservierte Control-Bytes | Bedeutung                                                    |
| ------------------------- | ------------------------------------------------------------ |
| b0000 000 0               | **General call adress**                                      |
| b0000 000 1               | Start Byte für langsame Geräte (haben mehr Zeit zum Antworten) |
| b1111 0xx                 | 10 Bit Adresse (xx Bit9 & Bit8, 2.Byte Bit7-0)               |

| General Call Adress                                          | General Hardware Call                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image.D81QM0](../../Typora.assets/image.D81QM0.png)        | ![image.68YVM0](../../Typora.assets/image.68YVM0.png)        |
| **B=0**  Second Byte:  0x06 (Reset and write programmable part of slave adress), 0x04 (Write programmable part of slave Adress) | **B=1**  Maser sendet in "second Byte" eigene Adresse, weil er Zieladresse nicht kennt |



*Maschinenbefehle (317-371)*



#### Adressierungsarten

| Immediate                     | Direct                                             | Register                                 | Indirect                                                     | Indexed                                                  | Bit                                  |
| ----------------------------- | -------------------------------------------------- | ---------------------------------------- | ------------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------ |
| Openrand in Code              | Adresse des Operanden in Code                      | Operand steht in Prozessorregister       | Ort der Adresse des Operanden in Code                        | Operand steht in Adresse aus Index+Referenz              | Ändert einzelne Bits eines Operanden |
| "Schreibe **5** ins Register" | "Schreibe Inhalt der Adresse **0xA5** in Register" | "Schreibe Inhalt von **AX** in Register" | "Schreibe Inhalt der Adresse der Adresse **0xF7** in Register" | "Schreibe Inhalt der Adresse **0x03+Index** in Register" | "Setze Bit5 von Register auf 0"      |



#### Prozessorarten

| CISC-Prozssor (Complex Instruction Set)                      | RISC-Processor (Reduced Instruction Set)                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Vielzahl verschiedener Befehle/Befehlsformate                | wenige grundlegende Befehle                                  |
| hohe Codedichte (komplexer Befehlsablauf pro Maschinenbefehl) | niedrige Codedichte (einfache Befehlsabläufe pro Maschinenbefehl) |
| Anzahl Bytes pro Befehl variabel                             | Anzahl Bytes pro Befehl = Verarbeitungsbreite                |
| unterschiedlich viele Taktzyklen pro Befehl                  | gleiche Anzahl von wenigen Taktzyklen pro Befehl             |
| wenige spezielle interne Register                            | großer interner Registersatz                                 |

```mermaid
graph LR
A(C-Code) -->|Compiler|B[Assembler-Code]
    B -->|Assembler| C(Binär-Code)
```



#### Phasen der Befehlsverarbeitung

| Fetch Instruction (FI)                            | Decode Instruction (DI) | Fetch Operand (FO)                  | Execute Instruction (EI) | Store Operand (SO)   |
| ------------------------------------------------- | ----------------------- | ----------------------------------- | ------------------------ | -------------------- |
| Instruction Pointer PC zeigt auf aktuellen Befehl | Befehl dekodieren       | Daten aus Speicher/IO in Rechenwerk | Daten verarbeiten        | Daten in Speicher/IO |

| Sequentielle Befehlsverarbeitung                             |
| ------------------------------------------------------------ |
| ![image-20200627110206412](../../Typora.assets/image-20200627110206412.png) |
| Pro Befehl werden die Bearbeitungs-Phasen (Fetch, Decode, Execute) <u>nacheinander</u> durchlaufen, dann beginnt der nächste Befehl |
| Befehl dauert 3 Zyklen z (t~v~=3z)                           |

| Überlappende Befehlsverarbeitung                             |
| ------------------------------------------------------------ |
| ![image-20200627110251262](../../Typora.assets/image-20200627110251262.png) |
| n = 3-Stufen Pipeline: Bearbeitungs-Phasen mehrerer Befehle werden <u>parallel</u> ausgeführt |
| Befehl dauert 1 Zyklus z (t~v~=1z = 1/n * t~r~)              |
| <img src="../../Typora.assets/image-20200627111034177.png" alt="image-20200627111034177" style="zoom:40%;" /> |

| Unterbrechung durch Daten                                    |
| ------------------------------------------------------------ |
| ![image-20200627132225528](../../Typora.assets/image-20200627132225528.png) |
| ESI+=1 liegt erst in z4 vor, wird aber gleichzeitig in z4 on [ESI] gebraucht<br />→ [ESI] wird in z5 verschoben |

| Unterbrechung durch SprungUnterbrechung durch Sprung         |
| ------------------------------------------------------------ |
| ![image-20200627132239968](../../Typora.assets/image-20200627132239968.png) |
| Sprung wird in z5 ausgeführt, alle vorher vorbereiteten Befehle werden verworfen<br />→ ab z6 folgen neue Befehle nach Sprung |



*Stacks & Unterprogramme (372 - 390)*

- Register, Heap, Stack
- Stack:  Aufsteigend-leer, Aufsteigend-voll, Absteigend-leer, Absteigend-voll
- Unterprogrammaufruf:  x86, Cortex-M



*Programmunterbrechungen (391 - 415)*



| Hardware-Interrupt                                           | Mehrere I/Os mit HI                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| I/O sendet Hardware-Interrupt-Request → Prozessor führt Interrupt Service-Routine (ISR) aus | 1. Interrupleitungen sind mit μP-Interrupt-Eingang OR-Verknüpft<br />2. Interrupt-Controller leitet einen vieler I/O-Eingänge an μP |

<img src="../../Typora.assets/image-20200628165754314.png" alt="image-20200628165754314" style="zoom: 33%;" />



#### DMA und Busmastering (416 - 449)

| DMA                                                   | Busmastering                                         |
| ----------------------------------------------------- | ---------------------------------------------------- |
| Datentransport IO / Speicher → IO / Speicher ohne CPU | Mehrere Busmaster an selbem Bus                      |
| einfache Bussteuerung                                 | Bussteuerung flexibler als DMA, erfordert Busarbiter |



*Cache-Speicher (450 - 478)*

![image-20200701150944284](../../Typora.assets/image-20200701150944284.png)

|         | TAG-RAM                           | Cacheline-Index                     | Speicherwort                        | Byteauswahl                   |
| ------- | --------------------------------- | ----------------------------------- | ----------------------------------- | ----------------------------- |
|         | Nummer der Speicherseite          | Nummer der Cacheline                | Nummer des Speicherworts            | Nummer des Bytes              |
| Inhalt  | mehrere Cachelines                | mehrere Speicherworte               | mehrere Bytes                       | 8 Bit                         |
| Adresse | 2 Bit                             | 2 Bit                               | 1 Bit                               | 1 Bit                         |
| Größe   | 2^2^=4 Speicherseiten im Speicher | 2^2^=4 Cachelines pro Speicherseite | 2^1^=2 Speicherwörter pro Cacheline | 2^1^=2 Bytes pro Speicherwort |



#### Häufige Fehler

- BE# sind <u>low active</u>
- Bus-Idle Phase bei I2C nicht vergessen

  - DAC:  aus 2^8^=25**6**  werden 25**5** Spannungsstufen!
- Position in Cacheline mit Wort & **Bytes**!
- Rechenweg aufschreiben
- I2C: Binär- und Hex-Wert aufschreiben, und Adress-/Daten-Bits aufschreiben (A6 .. A0 / D7 ... D0)
- Memory-Map: Gesamt-Adresse läuft in 2er-/4er-...Schritten → höchste Gesamt/Speicherwort-Adresse ist kleiner als höchste Adresse in Speicher
- Memory-Map: Adress-Schritte in ICs einzeichnen (erste **2** Adresse & letzte Adresse)
- Memory-Map: IC's nach Speicher-Art bezeichnen (z.B. **EEPROM1**) und IC-Größe hineinschreiben
- Memory-Map: in ungenutztem Speicherbereich auch maximale Adresse kennzeichnen (größer als Gesamt-Adresse)