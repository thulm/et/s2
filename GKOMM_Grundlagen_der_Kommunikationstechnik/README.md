# LSFKurzbezeichnung(NR)_Name
Bitte Regeln im [README.md](https://gitlab.com/thulm/et/s1/-/blob/main/README_ANKI.md) vom 1. Semester Beachten.

### Erfahrungen/Tipps @ThorbenCarl WS20 
#### Vorlesung
- Sehr gut! Top organisiert und gutes Skript, macht Spaß
#### Klausur
- Klausur machbar

### Erfahrungen/Tipps David Sievers WS20
#### Vorlesung
- Quasi das Selbe wie in DT1, sehr gut gemacht bei anderem Stoff
#### Klausur
- 

### Erfahrungen/Tipps 
#### Vorlesung Georg Missel SS21
- Datenübertragungen werden analysiert, vom Internetprotokoll werden die Datenpakete analysiert, CAN-Datenpakete analysieren. Viel Arbeit bei der Formelsammlung, weil viele kleine Themen angesprochen werden.
#### Klausur
- Dementsprechend umfangreiches Wissen wird abgefragt. Aber ist gut machbar

### Erfahrungen/Tipps @DanielGlaeser WS21
#### Vorlesung
- Wie in DT1 sehr schön umgesetzt. Sehr interessant. Gegen Ende jedoch viele Folien direkt aus der Vorlesung im Skript ohne großartige Erklärung.
Sehr interessantes Labor. Alles in allem sehr interessant und informativ.
#### Klausur
- Klausur ist sehr fair. Vorbereitungsunterlagen etwas dürftig, da einfaches Thema – Bei Bedarf Pross Klausuren ansehen.