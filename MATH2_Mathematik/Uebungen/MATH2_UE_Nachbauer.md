![Typora MA2 Aufgaben Export Settings](../../Typora.assets/Typora%20MA2%20Aufgaben%20Export%20Settings.png)

[TOC]

$$ \def\phase#1{\underline{\big/#1}} \def\laplace{○—\!\!●} \def\Laplace{●\!—○} \def\Sha{Ш} $$

<div style="page-break-after: always;"></div>

# ————— D G L  —————





## 1\. Einzelne Differentialgleichungen



### Aufgabe DGL-11 (Getrennte Veränderliche)

* [ ] $\dot x = \sin(t)\cdot x\qquad x(0) = -1$
* [ ] $y' = \dfrac{x}{(y+1)^2}\qquad y(0) = 0$



Die Beschleunigung eines Lamborghini ist proportional zur Differenz zwischen 250 $\frac{km}{h}$ und der aktuellen Geschwindigkeit. Es ist bekannt, dass er in 10s von 0 $\frac{km}{h}$ auf 108 $\frac{km}{h}$ beschleunigen kann.

* [ ] Welches Anfangswertproblem beschreibt die Geschwindigkeit eines Lamborghini, der aus der Ruhe heraus maximal beschleunigt?
* [ ] Löse es. Bestimme insbesondere die Proportionalitätskonstante.
* [ ] Wie lange dauert es, bis er 180 $\frac{km}{h}$ erreicht?

<div style="page-break-after: always;"></div>

### Aufgabe DGL-12 (1D homogen)

Bestimme die Lösungen von:

* [ ] $\ddot x - 5\dot x + 6x = 0\qquad x(0) = 0\qquad\dot x(0) = 2$
* [ ] $\ddot{x} + 3\dot x + 2x = 0\qquad x(0) = 3\qquad\dot x(0) = 0$
* [ ] $y'''' - 4y''' + 16y' - 16y = 0$



### Aufgabe DGL-13 (1D inhomogen)

Bestimme die <u>spezielle</u> Lösung von:

* [ ] $\ddot x + 3 \dot x + 2x = 2e^{-3t}$
* [ ] $\dot x + 2x = \sin(t)$
* [ ] $\dot x + 2x = t^2$
* [ ] $\ddot x+2\dot x+10x=10\cdot\cos(4t)$
* [ ] $\ddot x+4\dot x+3x=5\cdot e^{-2t}$
* [ ] $\ddot x + 4\dot x+4x=-8t^2$



### Aufgabe DGL-13 (1D gesamt)

Löse folgende DGL vollständig

* [ ] $\ddot{x} +4\dot{x} +13x = 39, \qquad x(0)=\dot{x}(0)=0$

<div style="page-break-after: always;"></div>

## 2\. DGl-Systeme



### Aufgabe DGL-21 (Transformation auf 1D)

Transformiere folgende AWP auf äquivalente Anfangswertprobleme als System 1. Ordnung.

* [ ] $\dddot x + 4t\ddot x + 2\dot x + 3x = \sin(2t)\\x(0) = 0\quad\dot x(0) = 0\quad\ddot x(0) = 2\\~$
* [ ] $\begin{matrix}\ddot x = -y + t && x(0) = 0, & \dot x(0) = 1 \\ \ddot y = \dot x + y &~& y(0) = 2, & \dot y(0) = 3\end{matrix}$



### Aufgabe DGL-23 (3D homogen)

Löse folgendes DGL-System mit der Eigenwertmethode.

$\quad \dot{\vec{x}} = \left(\begin{matrix} 1 &3 &3\\ 2 &0 &2\\ 0 &10 &2\end{matrix}\right)\cdot \vec{x}$



### Aufgabe DGL-24 (2D inhomogen)

Ermittle die Lösungsgesamtheit von

$\quad\dot{\vec{x}} = \left(\begin{matrix}-2 & -8 \\4 & 6\end{matrix}\right)\cdot \vec{x} + \left(\begin{matrix}0 \\-4e^{-2t}\end{matrix}\right)$

mit $x_0(0)=1$ und $x_1(0) = 2$

<div style="page-break-after: always;"></div>

## 4\. Differenzen-Gleichung



### Aufgabe DGL-41 (DZGL homogen)

Bestimme die Lösungsgesamtheit von

* [ ] $y[n+2] - 2y[n+1] + 2y[n]=0$
* [ ] $y[n+3] - 2y[n+2] - 4y[n+1] + 8y[n] = 0$



### Aufgabe DGL-42 (DZGL inhomogen)

Bestimme die spezielle Lösung von

* [ ] $y[n+1] - 2y[n] = 6n^2$
* [ ] $y[n+2]+2y[n+1]-3y[n]=2\cdot 3^n$
* [ ] $y[n+2] + 4y[n] = 6\sin\left(n\frac{\pi}{2}\right)$

<div style="page-break-after: always;"></div>

## 5\. Numerisch



### Aufgabe DGL-51 (Diskretisierung)

Wir betrachten das Anfangswertproblem $\dot{x} = -2x + 5t, \quad x(0)=5$.

* [ ] Führe die Vorwärtsdiskretisierung mit $h=0.2$ durch, und berechne $x(0.2)$ und $x(0.4)$.
* [ ] Führe die zentrale Diskretisierung mit $h=0.4$ durch, und berechne $x(0.8)$. Entnimmm $x(0.4)$ der vorigen Teilaufgabe.



### Aufgabe DGL-52 (Runge Kutta)

Wir beschäftigen uns mit dem Anfangswertproblem

$\quad\dot x = -2x + 8t$, $x(0) = 2$

* [ ] Löse es exakt.
* [ ] Führe von Hand einen Schritt des Runge-Kutta-Verfahrens mit $h=\frac{1}{2}$ durch.


Führe für das Anfangswertproblem

$\quad\dot x=4y,\qquad\qquad x(0)=0\\\quad\dot y=-2x+4t,\qquad y(0)=-1$

* [ ] einen Schritt des Runge-Kutta-Verfahrens mit $h=1$ durch

<div style="page-break-after: always;"></div>

## 6\. Jordansche Normalform



### Aufgabe DGL-61 (Übersicht möglicher Kombinationen)

Welche möglichen Kombinationen an Eigenwerten, Eigenvektoren und Hauptvektoren gibt es bei einer 3x3-Matrix? Welche Jordan-Matrix und welches Minimal-Polynom resultiert daraus jeweils?



### Aufgabe DGL-62 (JNF berechnen)

$A=\begin{pmatrix} 4 & 1 & 1 \\ -2 & 1 & -2 \\ 1 & 1 & 4 \end{pmatrix} \\~$

* [ ] Berechne die Transformationsmatrix S
* [ ] Bestimme die zugehörige Jordan Normalform J
* [ ] Ermittle das Minimalpolynom

<div style="page-break-after: always; break-after: page;"></div>

### Aufgabe DGL-63a (JNF ablesen)

$A=\begin{pmatrix} 0&-3&-1&1\\ 1&-4&-1&1\\ 0&1&-1&-1\\ 1&-3&-1&0 \end{pmatrix}\\~$ 			$\text{Eigenwerte:}\\\begin{matrix}\lambda_1=\lambda_2=\lambda_3=-1\\\lambda_4=-2\end{matrix}$

Außerdem gilt für $B_{-1}=A-(-1)I\qquad B_{-2}=A-(-2)I$

$\\B_{-1}=\begin{pmatrix} 1&-3&-1&1\\ 1&-3&-1&1\\ 0&1&0&-1\\ 1&-3&-1&1 \end{pmatrix}\quad B_{-2}=\begin{pmatrix} 2&-3&-1&1\\ 1&-2&-1&1\\ 0&1&1&-1\\ 1&-3&-1&2 \end{pmatrix}$

$\\B_{-1}\cdot B_{-2}=\begin{pmatrix} 0&-1&0&1\\ 0&-1&0&1\\ 0&1&0&-1\\ 0&-1&0&1 \end{pmatrix}\quad{ B_{-1}}^2=\begin{pmatrix} -1&2&1&0\\ -1&2&1&0\\ 0&0&0&0\\ -1&2&1&0\ \end{pmatrix} $

$\\ {B_{-1}}^2\cdot B_{-2}=\begin{pmatrix} 0&0&0&0\\ 0&0&0&0\\ 0&0&0&0\\ 0&0&0&0\\ \end{pmatrix}\\~$

* [ ] Bestimme die Transformationsmatrix S
* [ ] Bestimme die zugehörige Jordan Normalform J
* [ ] Ermittle das Minimalpolynom

<div style="page-break-after: always; break-after: page;"></div>

### Aufgabe DGL-63b (JNF ablesen)

$A=\begin{pmatrix} -1&0&-1&0\\ -1&-1&1&1\\ 3&-1&-5&-1\\ -7&3&6&0 \end{pmatrix}\\~$ 			$\text{Eigenwerte:}\\\begin{matrix}\lambda_1=\lambda_2=\lambda_3=-2\\\lambda_4=-1\end{matrix}$

Außerdem gilt für $B_{-1}=A-(-1)I\qquad B_{-2}=A-(-2)I$

$\\B_{-1}=\begin{pmatrix} 0&0&-1&0\\ -1&0&1&1\\ 3&-1&-4&-1\\ -7&3&6&1 \end{pmatrix}\quad B_{-2}=\begin{pmatrix} 1&0&-1&0\\ -1&1&1&1\\ 3&-1&-3&-1\\ -7&3&6&2 \end{pmatrix}$

$\\{B_{-2}}^3=\begin{pmatrix} -4&2&3&1\\ -8&4&6&2\\ 0&0&0&0\\ -4&2&3&1\\ \end{pmatrix}\quad B_{-2}\cdot B_{-1}=\begin{pmatrix} -3&1&3&1\\ -5&2&4&1\\ -1&0&2&1\\ 1&0&-2&-1 \end{pmatrix}$

$\\{B_{-2}}^2\cdot B_{-1}=\begin{pmatrix} -2&1&1&0\\ -2&1&1&0\\ -2&1&1&0\\ 2&-1&-1&0 \end{pmatrix}\\~$

* [ ] Bestimme die Transformationsmatrix S
* [ ] Bestimme die zugehörige Jordan Normalform J
* [ ] Ermittle das Minimalpolynom

<div style="page-break-after: always; break-after: page;"></div>

### Aufgabe DGL-63c (JNF ablesen)

$A=\begin{pmatrix} 2&1&2&2\\ 0&2&1&1\\ 1&0&2&1\\ -1&0&-1&0 \end{pmatrix}\\~$ 			$\text{Eigenwerte:}\\\begin{matrix}\lambda_1=\lambda_2=1\\\lambda_3=\lambda_4=2\end{matrix}$

Außerdem gilt für $B_{1}=A-1I\qquad B_2=A-2I$

$\\B_1=\begin{pmatrix} 1&1&2&2\\ 0&1&1&1\\ 1&0&1&1\\ -1&0&-1&-1 \end{pmatrix}\quad B_2=\begin{pmatrix} 0&1&2&2\\ 0&0&1&1\\ 1&0&0&1\\ -1&0&-1&-2 \end{pmatrix}$

$\\{B_2}^2\cdot B_1=\begin{pmatrix} 0&0&0&0\\ 0&0&0&0\\ 0&0&0&0\\ 0&0&0&0 \end{pmatrix}\quad B_{1}\cdot B_{2}=\begin{pmatrix} 0&1&1&1\\ 0&0&0&0\\ 0&1&1&1\\ 0&-1&-1&-1 \end{pmatrix}$

$\\ {B_1}^2~ B_{2}=\!\begin{pmatrix} 0&1&1&1\\ 0&0&0&0\\ 0&1&1&1\\ 0&-1&-1&-1 \end{pmatrix}\quad {B_2}^2=\!\begin{pmatrix} 0&0&-1&-1\\ 0&0&-1&-1\\ -1&1&1&0\\ 1&-1&0&1 \end{pmatrix} \\~$

* [ ] Bestimme die Transformationsmatrix S
* [ ] Bestimme die zugehörige Jordan Normalform J
* [ ] Ermittle das Minimalpolynom

<div style="page-break-after: always; break-after: page;"></div>

### Aufgabe DGL-63d (JNF aufstellen)

Gegeben ist eine 5x5-Matrix mit den Eigenwerten:

​	$-1$ ist dreifacher EW mit geometrischer Vielfachheit 3

​	$4$ ist doppelter EW mit geometrischer Vielfachheit 1



* [ ] Notiere das charakteristische Polynom $\chi(\lambda)$
* [ ] Bestimme die zugehörige Jordan Normalform J
* [ ] Ermittle das Minimalpolynom $m_A(z)$





<div style="page-break-after: always;"></div>

# ——— S I G N A L E ———





## 1\. Komplexe Zahlen



### Aufgabe SIG-11 (Komplexe Zahlen)

* [ ] $z=\dfrac{3+j}{-4+2j}$
* [ ] $z=2\underline{\big/40\degree} + 3\underline{\big/110\degree}$
* [ ] $z=\left(\dfrac{1}{2} + j\right)^{10}$
* [ ] $z=\dfrac{2+1\underline{\big/40\degree}}{3-2\underline{\big/50\degree}}$
* [ ] $z^3=\frac{1}{8}\underline{\big/30\degree}$



### Aufgabe SIG-13 (Komplexe Polynome)

Eine Lösung der Gleichung $z^4 - 4z^3 + 11z^2 - 14z + 10=0$ ist gegeben durch $z_1 = 1 + j$.

Bestimme sämtliche Lösungen.

<div style="page-break-after: always;"></div>

## 2\. Periodische Funktionen



### Aufgabe SIG-22 (Spektraldarstellung)

Wir interessieren uns für

$ X_e(t) = 2 + \left(2\cos(10^3t) - \sin(10^3t)\right)\qquad\\~\qquad\qquad +\left(\cos(2\cdot 10^3t)- \sin(2\cdot 10^3t)\right)\\ + \sin(3\cdot 10^3t)\qquad\quad\\~ $

* [ ] Notiere die zugehörige komplexe Darstellung.
* [ ] Zeichne das Amplitudenspektrum
* [ ] Zeichne das auf den $\cos$ bezogene Phasenspektrum



### Aufgabe SIG-24 (Sprungstellenanalyse)

Berechne für $0 < a < 1$ die Fourierreihe der folgenden p-periodischen Funktion:

![image-20221010213338492](../../Typora.assets/image-20221010213338492.png)

<div style="page-break-after: always;"></div>

## 3\. Fourriertransformation



### Aufgabe SIG-32 (Fourier Paare)

Berechne jeweils die Fourier-Transformierte:

* [ ] $x(t)=e^t\cdot\varepsilon(-t)$
* [ ] $x(t)=t\qquad 0\le t\le1$
* [ ] $x(t)=1\qquad 0\le t\le T$



Weise nach, dass

* [ ] $\sin(\omega t)\quad\laplace\quad\pi j\bigg(\delta(\omega+\omega_0)-\delta(\omega-\omega_0)\bigg)$

<div style="page-break-after: always;"></div>

### Aufgabe SIG-34 (Fourier Verschiebung)

Skizziere jeweils die Fourier-Transformierte:

* [ ] $x(t)=3\cdot\cos(t)$
* [ ] $x(t)\cdot e^{j4t}$
* [ ] $x(t)\cdot\sin(t)$



* [ ] $x(t)\cdot(2+2\cos(5t))$



* [ ] Berechne ausgehend von $e^{-t}\cdot\varepsilon(t)\quad\laplace\quad\dfrac1{1+j\omega}$ die Fourier-Transformierte von $2~e^{-t+2}\cdot\varepsilon(t-2)$
* [ ] Berechne ausgehend von $rect(\frac tT)\quad\laplace\quad T\cdot si(\frac{\omega T}2)$ die Fourier-Transformierte von

![image-20221010213419402](../../Typora.assets/image-20221010213419402.png)

<div style="page-break-after: always;"></div>

## 4\. Laplacetransformation



### <font size="6">Aufgabe SIG-40a (Laplace Formel→Formel)</font>

Transformiere mit Hilfe von Laplace.

* [ ] $\dfrac{5s+3}{s^2+s}$
* [ ] $\left[-e^{-t}+2\cos(2t)+\dfrac12\sin(2t)\right]\cdot \varepsilon(t)$
* [ ] $\left[3t-1+e^{-3t}\right]\cdot \varepsilon(t)$



### <font size="6">Aufgabe SIG-40b (Laplace Skizze→Formel)</font>

* [ ] Transformiere nachfolgendes Signal mithilfe von Laplace in den Frequenzbereich. ![image-20221010213446194](../../Typora.assets/image-20221010213446194.png)
* [ ] Transformiere nachfolgendes Signal mithilfe von Laplace in den Frequenzbereich. ![image-20221010213515374](../../Typora.assets/image-20221010213515374.png)

<div style="page-break-after: always;"></div>

### <font size="6">Aufgabe SIG-40c (Laplace Formel→Skizze)</font>

* [ ] $\frac{1}{s} + \frac{1}{s^2}e^{-s} + \left(\frac{1}{s} - \frac{1}{s^2}\right)e^{-3s}~\Laplace$  Skizze im Zeitbereich?
* [ ] $\frac{1}{s^2} + \left(\frac{1}{s^2} - \frac{2}{s}\right)e^{-s} - \frac{2}{s^2}e^{-4s}~\Laplace$  Skizze im Zeitbereich?















## 5\. Z-Transformation



### Aufgabe SIG-50 (Z-Transformation)

Berechne ausgehend von

$x[n] \quad\laplace\quad X(z)\\~$

* [ ] $z^3X(z)\quad \Laplace\quad$ ?
* [ ] $z^2X(\frac{1}{z})\quad \Laplace\quad$ ?
* [ ] $X(z)^2 \quad\Laplace\quad$ ?

<div style="page-break-after: always;"></div>

## 6\. Diskrete Fourrier-Transformation



### Aufgabe SIG-62 (DFT)

Gegeben ist die $\dfrac\pi2$-periodische Funktion

![image-20221010214006614](../../Typora.assets/image-20221010214006614.png)

Berechne gestützt auf die DFT mit N= 8 näherungsweise die 2. Oberschwingung von x in reeller Darstellung.

<div style="page-break-after: always;"></div>

### Aufgabe SIG-64 (DFT IDFT)

* [ ] Berechne von der unten stehenden Zeichnung die ersten drei Oberschwingungen mit Hilfe der DFT unter Zugrundelegung von 8 Stützstellen. ![image-20221010213845450](../../Typora.assets/image-20221010213845450.png)
* [ ] Jemand hat mit Hilfe der DFT unter Zugrundelegung von 6 Stützstellen folgende Werte erhalten:

  $\hat{f}_0 = 4 \quad\hat{f}_1 = 1+j \quad\hat{f}_2 = 2j\quad \\ \hat{f}_3 = 0 \quad\hat{f}_4 = -2j \quad\hat{f}_5 = 1-j$

  Rekonstruiere die ersten beiden Werte der ursprüngliche Folge.

<div style="page-break-after: always;"></div>

### Aufgabe SIG-66a (FFT)

Jemand führt die FFT für $N=2^6$ durch.

* [ ] Notiere die zugehörige Beklammerung
* [ ] Wie viele Butterflys enthält der dazugehörige Signalflussgraph?
* [ ] In welcher Position steht Eingang Nr. 27, wenn die Ausgänge in natürlicher Reihenfolge erscheinen sollen?



### Aufgabe SIG-66b (FFT)

berechne die FFT für $\quad\begin{pmatrix}1\\-2\\3\\2\end{pmatrix}\quad\laplace\quad$ ?



### Aufgabe SIG-67 (FFT)

Jemand will die FFT für $N = 6 = 3\cdot 2$ wie angedeutet durchführen.

* [ ] Notiere die zugehörige Beklammerung
* [ ] Zeichne den Signalflussgraph
* [ ] Fülle ihn aus für $f_0=0$, $f_1=2$, $f_2=1$, $f_3=0$, $f_4=-1$, $f_5=-2$

<div style="page-break-after: always;"></div>

## 7\. Analoges - Diskretes Spektrum



### Aufgabe SIG-70a

Gegeben ist $x(t)=3-\cos(2t)$

* [ ] Skizziere die Fouriertransformierte von $x(t)$
* [ ] Skizziere die Fouriertransformierte von $x(t)\cdot\Sha_{\frac{\pi}3}(t)$



### Aufgabe SIG-70b

Ein Signal $x(t)$ besitzt die Fourier-Transformierte
$$
\hat{x}(\omega) = \left\{\begin{array}{ll} 0 & \omega < -2 \\ 4-\omega^2 & -2 \leq \omega \leq 2 \\ 0 & \omega > 2\end{array}\right.
$$


* [ ] Skizziere $\hat{x}(\omega)$ wenn man $x(t)$ mit $\Delta t = \frac{\pi}{3}$ abtastet.
* [ ] Skizziere $\hat{x}(\omega) \cdot \Sha_{0.5}(\omega)$.
* [ ] Skizziere die Fouriertransformierte von $\left[x(t)\circledast\Sha_{4\pi}(t)\right]\cdot\Sha_{\frac{\pi}3}(t)$

<div style="page-break-after: always;"></div>

## 8\. Splines



### Aufgabe SIG-80a (Klassischer Spline)

Durch die Punkte mit den Koordinaten

$(-1, -2)$, $(0,0)$, $(1,0)$, $(2, -2)$, $(4,-4)$

soll ein klassischer Spline $S$ mit den Randbedingungen $S'(-1)=3$, $S'(4) = 5$ gelegt werden.

* [ ] Berechne die Steigungen in den Stützstellen.
* [ ] Ermittle das aus $[0,1]$ zuständige Teil-Polynom.



### Aufgabe SIG-80b (Extrapolations-Spline)

Durch die Punkte mit den Koordinaten

$(0, 4)$, $(0.5,4)$, $(1,3)$, $(1.5, 1)$, $(2,-2)$, $(2.5,-6)$

soll ein Extrapolationsspline gelegt werden.

* [ ] Ermittle die Steigungen in den gegebenen Punkten.
* [ ] Ermittle das aus $[1,1.5]$ zuständige Teil-Polynom.

<div style="page-break-after: always;"></div>

## 9\. Zufallssignale



### Aufgabe SIG-90a

Gegeben ist das diskrete Signal:

$x[0] = 4,\quad x[1] = 2\quad x[2] = 0\quad x[3] = -1\\ x[i] = 0\qquad\forall i > 3\\~$

Berechne $\phi_{xx}$ und $S_{xx}$, wobei $S_{xx}$ reell dastehen soll.



### Aufgabe SIG-90b

Ein lineares diskretes Filter wird durch folgende Differenzengleichung beschrieben

$y[n]=2x[n]+2x[n-1]+x[n-2]\\~$

Am Eingang liegt die Musterfunktion eines stationären Prozesses mit der AKF an

$\phi_{xx}[0] = 4,\quad\phi_{xx}[\pm 1] = 2,\quad\phi_{xx}[\pm 2] = 1,\\\phi_{xx}[i] = 0 \qquad \forall i\\~$

Berechne die AKF $\phi_{yy}$ und die Leistungsdichte $S_{yy}$ des Ausgangs $y[n]$.

<div style="page-break-after: always;"></div>

## B. Wavelets



### Aufgabe SIG-Ba (Wavelet)

Berechne die Wavelet-Koeffizienten und den Gleichanteil
$$
x:\qquad8\quad6\quad5\quad1\qquad-1\quad-1\quad0\quad2
$$


### Aufgabe SIG-Bb (Wavelet Invers)

Rekonstruiere die ursprünglichen Werte
$$
\text{Gleichanteil: }2\qquad\begin{matrix}-1\\-3&0.1\\1&2&0.2&0\end{matrix}
$$


<div style="page-break-after: always;"></div>

---

# ——— S Y S T E M E ———





## 1\. 2. Stetige Systeme



### Aufgabe SYS-10a (Zeiger Spannung)

Gegeben ist die folgende Schaltung. Die Eingangsspannung beträgt $u_e(t)=20V\cdot\sin(10kHz~t)$. Berechne die stationäre Ausgangsspannung $u_a(t)$

![image-20221010212339673](../../Typora.assets/image-20221010212339673.png)

<div style="page-break-after: always;"></div>

### Aufgabe SYS-10b (Zeiger Strom)

Gegeben ist die folgende Schaltung. Die Eingangsspannung beträgt $u(t) = 10V \cdot\sin(10^3s^{-1}t)$. Bestimme die stationären Ströme $i_1$, $i_2$ und $i_3$.

![image-20221010212316551](../../Typora.assets/image-20221010212316551.png)



### Aufgabe SYS-11 (Frequenzgang stetig)

Am Eingang des Filters liegt die folgende Spannung an:

$u_e(t)=2+6\cos(10t)-3\cos(20t)+\sin(20t)\\~$

![image-20221010212449331](../../Typora.assets/image-20221010212449331.png)

* [ ] Bestimme den Frequenzgang der Schaltung
* [ ] Welche stationäre Spannung $u_a$ liegt am Ausgang an?

<div style="page-break-after: always;"></div>

### Aufgabe SYS-12

Am Eingang des Filters liegt die Spannung $u_e(t) = \sin(4t)\cdot\varepsilon(t)$. In dieser Aufgabe soll die Lösungsgesamtheit der Ausgangsspannung $u_a(t)$ bestimmt werden.

![image-20221010212522042](../../Typora.assets/image-20221010212522042.png)

* [ ] Stelle die Differentialgleichung, die das dynamische Verhalten des Schwingkreises beschreibt auf.
* [ ] Bestimme die homogene und partikuläre Lösung von $x(t)$.
* [ ] Überlege Anfangsbedingungen und berechne die Lösungsgesamtheit.

<div style="page-break-after: always;"></div>

### Aufgabe SYS-13 (periodischer Eingang)

Wir beschäftigen uns mit nachfolgender Schaltung

![image-20221010212821533](../../Typora.assets/image-20221010212821533.png)

Berechne das Ausgangssignal für die Eingangssignale

* [ ] Rechtecksignal: ![image-20221010212857417](../../Typora.assets/image-20221010212857417.png)
* [ ] Trapezsignal: ![image-20221010212921674](../../Typora.assets/image-20221010212921674.png)

<div style="page-break-after: always;"></div>

### Aufgabe SYS-20a

Gegeben ist folgende Schaltung:

![image-20221010212706339](../../Typora.assets/image-20221010212706339.png)

* [ ] Notiere die Übertragungsfunktion $\ddot U(s)$
* [ ] Notiere die DGL, die Ausgang und Eingang verbindet
* [ ] Notiere den Frequenzgang
* [ ] Berechne Impulsantwort
* [ ] Berechne die Sprungantwort



### Aufgabe SYS-20b

Ein Netzwerk besitzt die Übertragungsfunktion $\ddot U(s)=\dfrac{6s}{(s+1)(s+3)}$

* [ ] Welche Differentialgleichung verbindet das Eingangssignal $x_e$ und das Ausgangssignal $x_a$?
* [ ] Berechne die Stoßantwort
* [ ] Berechne die Sprungantwort

<div style="page-break-after: always;"></div>

## 3\. Diskrete Systeme



### Aufgabe SYS-32

Ein diskretes Filter hat die Übertragungsfunktion $H(z) = \dfrac{1}{z^2 + z + \frac{3}{16}}$

* [ ] Notiere die zugehörige DZGL.
* [ ] Berechne jeweils die ersten 5 Werte von Stoß- und Sprungantwort mit Hilfe der DZGL.
* [ ] Berechne Stoß- und Sprungantwort allgemein.



### Aufgabe SYS-33 (Frequenzgang diskret)

Ein diskretes Filter wird durch folgende DZGL beschrieben: $y[n+1] - \frac{1}{2}y[n] = x[n]$

- [ ] Notiere die Übertragungsfunktion
- [ ] Berechne den Frequenzgang
- [ ] Berechne das stationäre Ausgangssignal zu  $x[n]=2\sin(n~\frac\pi2)$

<div style="page-break-after: always;"></div>

## 4\. Faltung



### Aufgabe SYS-41 (Faltung diskret)

Ein diskretes System hat die Stoßantwort:

$h_0 = 2, h_1=4, h_2 = 3, h_3 = 1, h_i = 0, i \geq 4$

* [ ] Bestimme die zugehörige Sprungantwort
* [ ] Berechne das Ausgangssignal zu  $x_{2i}= 1,~x_{2i+1}=-1, ~i\geq 0$
* [ ] Ein unbekanntes Eingangssignal erzeugt das Ausgangssignal $y_0 = 4,\quad y_1 = 12,\quad y_2 = 12,\quad y_3 = 2,\quad y_4 = -5\\y_5 = -4,\quad y_6 = -1,\qquad\qquad\qquad y_i = 0,\quad i > 6$ 
  Bestimme es.



### Aufgabe SYS-45 (Faltung Zeit)

Gegeben sind die folgenden beiden Signale, skizziere $x(t)\ast y(t)$.

![image-20221010214049948](../../Typora.assets/image-20221010214049948.png)



### Aufgabe SYS-46 (Faltung Frequenz)

![image-20221010214116651](../../Typora.assets/image-20221010214116651.png)

Berechne und skizziere die Fourier-Transformierte von

* [ ] $x(t)\cdot e^{-2jt}$
* [ ] $x(t)\cdot (2+\cos(3t))$

<div style="page-break-after: always;"></div>

## 5\. Korrelation



### Aufgabe SYS-51 (Korrelation diskret)

Gegeben sind

$\begin{matrix}x(t) & = & 4\delta(t+1) + 3\delta(t) + 2\delta(t-1) + \delta(t-2) \\ y(t) & = & 2\delta(t+2) + 2\delta(t+1) + \delta(t)\end{matrix}$



* [ ] Berechne und skizziere  $\phi_{yy}$
* [ ] Berechne und skizziere  $\phi_{xy}$
* [ ] Berechne  $\phi_{yx}$



### Aufgabe SYS-53 (Korrelation stetig)

Gegeben sind $\qquad\begin{matrix}x(t)&=&\varepsilon(t)-\varepsilon(t-3)\\y(t)&=&2\delta(t+1)-\delta(t-3)\end{matrix}$



* [ ] Berechne und skizziere  $\phi_{xx}$
* [ ] Skizziere  $\phi_{xy}$

<div style="page-break-after: always;"></div>

# EXTRAS & KLAUSURAUFGABEN



### Eigenvektoren bestimmen (3x3 Matrix mit doppeltem EW)

Bestimme die Eigenwerte, Eigen- und ggf. Hauptvektoren der Matrizen:

- [ ] $A=\begin{pmatrix}4&2&2\\-1&1&-1\\3&3&5\end{pmatrix}\\~$
- [ ] $A=\pmatrix{1&2&3\\4&3&-2\\0&0&5}$

<div style="page-break-after: always;"></div>

### Aufgabe KLA-16sG

Ein System besitzt die Sprungantwort  $x(t)=\begin{cases}0&t<0\\t^2&0\le t\le2\\4&t>2&\end{cases}$

- [ ] Berechne und skizziere die Stoßantwort.
- [ ] Berechne die Stoßantwort näherungsweise durch Abtasten der Sprungantwort
  mit Δt = ½.



### Aufgabe KLA-16sR (4x4 JNF von Hand)

![image-20230101212928692](../../Typora.assets/image-20230101212928692.png)

<div style="page-break-after: always;"></div>

### Aufgabe KLA-17wN (4x4 JNF)

![image-20230101212154294](../../Typora.assets/image-20230101212154294.png)

- [ ] Notiere gestützt darauf eine Produktdarstellung des Minimalpolynom von A, eine Jordansche Normalform J sowie ein S mit  $S^{−1}A~S =J$

<div style="page-break-after: always;"></div>

### Aufgabe KLA-19sG (PBZ komplexe Nullstelle)

- [ ] *als Vorübung: bestimme die Nullstellen  $s_1~~s_2$  mit  $s^2-4s+13\overset{!}{=}0$*
- [ ] Berechne die Laplace-Inverse von:  $\check f(s)=\dfrac5{s~(s^2+2s+5)}$



### Aufgabe KLA-20sCb

![image-20230101211927303](../../Typora.assets/image-20230101211927303.png)

- [ ] Notiere die Sprungstellentabelle

<div style="page-break-after: always;"></div>

### KLA-20sLb

Ein lineares diskretes Filter besitzt die Stoßantwort

$h[0]=-1\qquad h[1]=2\qquad h[2]=1\qquad h[i]=0\qquad i\ge3$

- [ ] Welches Eingangssignal gehört zum Ausgangssignal:
  $y[0]=-2\qquad y[1]=4\qquad y[2]=3\qquad y[3]=-2\\y[4]=-1\qquad y[i]=0\qquad i\ge5$



### Aufgabe KLA-20wL

Ein lineares diskretes Filter wird durch folgende Differenzengleichung beschrieben:

$y[n+1]-\frac12~y[n]=8~x[n]\qquad n\ge-1\qquad y[i]=0\qquad i<0$

- [ ] Berechne y[0], …, y[3] anhand der Differenzengleichung für
  ![image-20230101211809033](../../Typora.assets/image-20230101211809033.png)
- [ ] Notiere die Übertragungsfunktion und den Frequenzgang.
- [ ] Berechne die Sprungantwort
- [ ] Berechne das stationäre Ausgangssignal zu  $x[n]=10~\cos(n\frac\pi2)~\varepsilon[n]$