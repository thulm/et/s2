# Offizielle Übungen
Unter den folgenden Links findest du veröffentlichte Übungen:
- Prof. Riederle: [THU Laufwerk](https://fs.hs-ulm.de/public/max/Download/mathe2/uebungen/)

# Übungs Videos
Unter den folgenden Links findest du Videos zu Übungen:
- Tim Nachbauer: [Cloud](https://0ytrr3zvx8hoqv2d.myfritz.net/nextcloud/index.php/s/fKyypK6czNcFyaE?path=%2FMathematik%2FMA2%20Aufgaben%20Videos)