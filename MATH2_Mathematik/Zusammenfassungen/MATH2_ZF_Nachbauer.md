[TOC]














# —————————  D G L  —————————



# 0. [Getrennte Veränderliche](https://youtu.be/Oa7a6rP8Zd4)

$\begin{matrix}\dot x(t)=f(x)·g(t)\\ x(t_0)=x_0\end{matrix}\quad→\quad\dfrac{dx}{dt}=f(x)·g(t)\quad→\quad f(x)^{-1}·dx=g(t)·dt\quad→\quad\int f(x)^{-1}dx=\int g(t)dt$



# 1. Einzeln

|                   Gegeben                   | $~\\ d_2\ddot x+d_1\dot x+d_0x=\boldsymbol{g(t)}\qquad\qquad x(t_0)=x_0\qquad\dot x(t_1)=x_1\\$ |
| :-----------------------------------------: | :----------------------------------------------------------: |
| Möglichkeiten<br />für  $\boldsymbol{g(t)}$ | $\overset{\text{homogen}\\}{g_h(t)=0}\qquad\overset{\text{Polynom}\\}{g_p(t)=P(t)}\qquad\overset{\text{e-Funktion}\\}{g_e(t)=c_0e^{s_0t}}\qquad\overset{\text{trigonom. Funtkion}\\}{g_t(t)=a_0\cos(t)+b_0\sin(t)}$ |



## 1.a) Homogene Lösung


$\quad\overset{g(t)\text{ vernachlässigen / gleich Null setzen}\\}{d_2\ddot x+d_1\dot x+d_0x=g(t)\overset!=0}\qquad→\qquad\overset{\text{Nullstellen berechnen}\\}{\chi(s)=d_2s^2+d_1s+d_0\overset!=0}\qquad→\qquad\overset{\text{gesucht}\\}{x_h(t)}$

| einfache Nullstelle              | komplexe Nullstelle                       | mehrfache Nullstelle               |
| -------------------------------- | ----------------------------------------- | ---------------------------------- |
| $s_1\ne s_2\ne… j$               | $s_{1/2}=a\pm bj$                         | $s_1=s_2$                          |
| $x_h(t)=c_1e^{s_1t}+c_2e^{s_2t}$ | $x_h(t)=e^{at}·(c_1\cos(bt)+c_2\sin(bt))$ | $x_h(t)=c_1e^{s_1t}+t·c_2e^{s_2t}$ |

<div style="page-break-after: always;"></div>

## 1.b) Spezielle Lösung


$\quad\overset{\text{zu berechnen, falls }g(t)~=\text{ Plynom / e-Fkt. / trig. Fkt.}\\}{g(t)=g_p(t)\quad/\quad g_e(t)\quad/\quad g_t(t)}\ne0\qquad→\qquad\overset{\text{gesucht}\\}{x_s(t)}$

| Polynom                                                      | e-Funktion                                                   | trigonom. Funtkion                                           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $d_2\ddot x+d_1\dot x+d_0x=P(t)$                             | $d_2\ddot x+d_1\dot x+d_0x=c_0e^{s_0t}$                      | $d_2\ddot x+d_1\dot x+d_0x\\\quad=a_0\cos(\omega_0 t)+b_0\sin(\omega_0 t)$ |
| für  $x$  Polynom mit gleichem Grad wie  $P(t)$  einsetzen, z.B. Grad 2:  $x=a~t^2+b~t+c$ | $x_s(t)=\frac1{d_2s^2+d_1s+d_0}\bigg\vert_{s=s_0}c_0e^{s_0t}$ | $\underline x=\frac1{d_2s^2+d_1s+d_0}\bigg\vert_{s=\omega_0 j}(a_0-b_0j)\\~\\~~=a_1+b_1j\quad●—\!○\quad x_s(t)$ |
| für  $\dot x$  usw. jeweilige Ableitung einsetzen, z.B.  $\dot x=2a~t+b$ | Wenn Ansatz fehlschlägt: $x_s(t)=...c_0te^{s_0t}$            | $x_s(t)=a_1\cos(\omega_0 t)+b_1\sin(\omega_0 t)$             |
| mit Koeffizienten-Vergleich nach a, b, c auflösen und in  $x_s(t)=at^2+bt+c$  einsetzen | Konstante ist wie  $e^0\ \rightarrow\  s_0=0$                |                                                              |



## 1.c) Lösungsgesamtheit

$\quad x(t)=x_h(t)+x_s(t)\qquad\qquad$		(falls  $g(t)=0$  →  $x_s(t)=0$)



## 1.d) Anfangswert

$\quad\overset{\text{gegeben: }~x(t),~\dot x(t)~\text{ bei }~t=0\\}{x(0)=x_1\qquad\dot x(0)=x_2}\qquad→\qquad\overset{\text{gesucht}\\}{c_1\quad c_2}$

| t=0  in  x(t)  und  ẋ(t)  einsetzen                          | LGS lösen                        |      |
| ------------------------------------------------------------ | -------------------------------- | ---- |
| $x(0)=\quad a~c_1+b~c_2+c=x_1\\\dot x(0)=\quad\underbrace{d~c_1+e~c_2+f=x_2}_{LGS}$ | nach c~1~ und<br />c~2~ auflösen |      |



## 1.e) Ergebnis


$\quad x(t)=x_h(t)+x_s(t)$	(mit eingesetztem c~1~ / c~2~)



<div style="page-break-after: always;"></div>

# 2. System

$\quad\begin{matrix}\dot x=d_0~x+d_1~y~+~g_1(t)\\\dot y=d_2~x+d_3~y~+~g_2(t)\end{matrix}\quad→\quad\begin{pmatrix}\dot x\\\dot y\end{pmatrix}=\begin{pmatrix}d_0&d_1\\d_2&d_3\end{pmatrix}\begin{pmatrix}x\\y\end{pmatrix} +\begin{pmatrix}g_1(t)\\g_2(t)\end{pmatrix}\quad→\quad\dot{\vec x}=\bold A~\vec x+\vec g(t)$

 

## 2.a) Homogene Lösung

$\quad\overset{\text{gegeben}\\}{\bold A}\qquad→\qquad\overset{\text{Eigenwerte}\\}{\lambda_1,~\lambda_2,~..}\qquad→\qquad\overset{\text{Eigen-/Hauptvektoren}\\}{\vec e,~\vec h,~\vec h'}\qquad→\qquad\overset{\text{gesucht}\\}{\vec x_h(t)}$

| einfacher Eigenwert                                          | komplexer Eigenwert                                          | [mehrfacher Eigenwert](https://www.uio.no/studier/emner/matnat/math/MAT2440/v11/undervisningsmateriale/genvectors.pdf) |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\lambda_1\ne\lambda_2\ne… j$                                | $\underline\lambda_{1/2}=a\pm bj$                            | $\lambda_1=\lambda_2$                                        |
| Eigenvektoren:<br /> $\vec {e_1}~\bigg/~\vec{e_2}$           | komplexer Eigenvektor:<br />$\underline\lambda_1=a+bj\quad→\quad\vec{\underline e_1}\\$<br />*(2. EV nicht berechnen)* | **I.**  mehrfacher EV:   $\alpha~\vec{e_1}+\beta~\vec{e_2}\\$<br />**II.**  EV & HV:   $\vec{e_1}~\bigg/~B_2·\vec{h_2}=\vec{e_2}$ |
| $\vec x_h(t)=c_1e^{\lambda_1t}\!\big(\vec e_1\!\big)\\\qquad\quad+ c_2e^{\lambda_2t}\!\big(\vec e_2\!\big)$ | $\vec x_h(t)=e^{a_0t}\!\big(c_1\vec c_1 +c_2\vec c_2\!\big)$ | **I.**  $\vec x_h(t)=c_1e^{\lambda_1t}\big( \vec e_1\big)+c_2 e^{\lambda_2t}\big(\vec e_2\big)\\$<br />**II.**  $\vec x_h(t)=c_1e^{\lambda_1t} \!\big(\vec e_1\!\big) +c_2e^{\lambda_2t}\! \big(t~\vec e_2+\vec h_2\!\big)$ |



**Komplexer Eigenwert / Eigenvektor**

$\quad\vec{\underline e_1}=\begin{pmatrix}a_1+b_1j\\a_2+b_2j\end{pmatrix}\qquad→\qquad \vec{c_1}=\begin{pmatrix}a_1\cos(bt)-b_1\sin(bt)\\a_2\cos(bt)-b_2\sin(bt)\end{pmatrix}\quad\bigg/\quad\vec{c_2}=\begin{pmatrix}a_1\sin(bt)+b_1\cos(bt)\\a_2\sin(bt)+b_2\cos(bt)\end{pmatrix}$



**mehrfacher Eigenwert / Eigenvektor**

| Eigvektor(en)                                                | HV(en)                       | Trick    $B_{..}=\big(A-\lambda_{..} I\big)$                 | homogene Lösung    $\boldsymbol{\vec x_h(t)}$                |
| ------------------------------------------------------------ | ---------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\lambda_1=\lambda_2\ne\lambda_3\\~\\\alpha~\vec e_1+\beta~\vec e_2\\$<br />*(LGS unterbestimmt)* | 2 EW<br />– 2 EV<br />= 0 HV | nicht nötig, da kein HV gesucht                              | $\quad c_1e^{\lambda_1t}\big(\vec e_1\big)\\~\\+ c_2e^{\lambda_2t}\big(\vec e_2\big)\\~\\+c_3e^{\lambda_3t}\big(\vec e_3\big)$ |
| $\lambda_1=\lambda_2\ne\lambda_3\\~\\\vec e_1\\$<br />*(LGS eindeutige Lsg)* | 2 EW<br />– 1 EV<br />= 1 HV | $B_2·B_2·\underbrace{B_3}_{\vec h_2}=B_3·\underbrace{B_2·B_2}_{\vec e_3}=\vec 0\\~\\\vec e_2=B_2·\vec h_2$ | $\quad c_1e^{\lambda_1t}\big(\vec e_1\big)\\~\\+c_2e^{\lambda_2t}\big(t~\vec e_2+\vec h_2\big)\\~\\ +c_3e^{\lambda_3t}\big(\vec e_3\big)$ |
| $\lambda_1=\lambda_2=\lambda_3\\~\\\vec e_1\\$<br />*(LGS eindeutige Lsg)* | 3 EW<br />– 1 EV<br />= 2 HV | nur gleiche EW  →  $\vec h'$  erraten<br />$~\\\vec h=B·\vec h'\qquad\vec e=B·\vec h$ | $\quad c_1e^{\lambda_1t}\big(\vec e\big)~~+~~c_2e^{\lambda_2t}\big(t~\vec e+\vec h\big)\\~\\ +c_3e^{\lambda_3t}\big(\frac{t^2}2~\vec e+t~\vec h+\vec h'\big)$ |

<div style="page-break-after: always;"></div>

## 2.b) Spezielle Lösung

$\quad\overset{\text{zu berechnen, falls }\vec g(t)~=\text{ Plynom / e-Fkt. / trig. Fkt.}\\}{\vec g(t)=\vec g_p(t)\quad/\quad\vec g_e(t)\quad/\quad\vec  g_t(t)}\ne0\qquad→\qquad\overset{\text{gesucht}\\}{\vec x_s(t)}$

| Polynom                                                      | e-Funktion                                                   | Trigo                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\vec g_p(t)=\binom{..+..t+..t^2}{..+..t+..t^2}$             | $\vec g_e(t)=\vec c_0e^{s_0t}$                               | $\vec g_t(t)=\vec a_0\cos(\omega t)+\vec b_0 \sin(\omega t)$ |
| $\vec x_s(t)=\begin{pmatrix}a_1+b_1t\\a_2+b_2t\end{pmatrix}\\\ \\\dot{\vec x}_s(t)=\begin{pmatrix}b_1\\b_2\end{pmatrix}$ | $\vec x_s(t)=\begin{pmatrix}a~e^{s_0t}\\b~e^{s_0t}\end{pmatrix}\\\ \\\dot{\vec x}_s(t)=\begin{pmatrix}s_0a~e^{s_0t}\\s_0b~e^{s_0t}\end{pmatrix}$ | $\begin{matrix}\dot x=d_1x+a_1\cos(\omega t)\\\dot y=d_2y+b_2\sin(\omega t)\end{matrix}\\\ \\○—\!●\quad\begin{matrix}\omega j\  \underline x=\underline x+a_1\\\omega j\  \underline y=\underline y-b_2 j\end{matrix}$ |
| LGS lösen:  $\dot{\vec x}_s=A\ \vec x_s+\vec g$              | LGS lösen:  $\dot{\vec x}_s=A\ \vec x_s+\vec g$              | $\begin{pmatrix}\underline x\\\underline y\end{pmatrix}\quad●—\!○\quad \vec x_s(t)$ |
| $\vec x_s(t)=\vec a+\vec bt$                                 | Konstante ist wie  $e^{0t}\ \rightarrow\  s_0=0$             |                                                              |



# 3. Laplace


$\quad\dot x(t)\quad○—\!●\quad s\ \check x(s)-x(0)\qquad\qquad\ddot x(t)\quad○—\!●\quad s^2\check x(s)-s\ x(0)-\dot x(0)$

---

DGL in Bildbereich transformiern	→	nach  $\check x$  umformen	→	zurück transformieren



## 3.b) Spezielle Lösung

$\quad\check x_s(t)=\frac1{d_2s^2+d_1s+d_0}\bigg\vert_{s=s_0}\check g(s)\qquad s_0$:  Nullstelle von  $\check g(s)$



<div style="page-break-after: always;"></div>

# 4. Differenzen-Gleichung

|                   Gegeben                   | $~\\ d_2~y[n+2]+d_1~y[n+1]+d_0~y[n]=\boldsymbol{g[n]}\qquad\qquad y[n_0]=y_0\qquad y[n_1]=y_1\\$ |
| :-----------------------------------------: | :----------------------------------------------------------: |
| Möglichkeiten<br />für  $\boldsymbol{g[n]}$ | $\overset{\text{homogen}\\}{g_h[n]=0}\qquad\overset{\text{Polynom}\\}{g_p[n]=P[n]}\qquad\overset{\text{reelle Potenz}\\}{g_r[n]=c_0·a^n}$ |



## 4.a) homogene Lösung


$\quad\overset{g(t)\text{ vernachlässigen / gleich Null setzen}\\}{d_2~y[n+2]+d_1~y[n+1]+d_0~y[n]=g[n]\overset!=0}\qquad→\qquad\overset{\text{Nullstellen berechnen}\\}{\chi(z)=d_2z^2+d_1z+d_0\overset!=0}\quad→\quad\overset{\text{gesucht}\\}{y_h[n]}$

| reelle Nullstelle                  | komplexe Nullstelle                                    | mehrfache Nullstelle                             |
| ---------------------------------- | ------------------------------------------------------ | ------------------------------------------------ |
| $z_1\ne z_2\ne\dots j$             | $z_{1/2}=a\pm bj=r·\underline{\big/\varphi}$           | $z_1=z_2$                                        |
| $y_h[n]=c_1\ {z_1}^n+c_2\ {z_2}^n$ | $y_h[n]=r^n·(c_1\cos(n\ \varphi)+c_2\sin(n\ \varphi))$ | $y_h[n]=c_1~{z_1}^n~+~\boldsymbol n·c_2~{z_2}^n$ |



## 4.b) spezielle Lösung


$\quad\overset{\text{zu berechnen, falls }g[n]~=\text{ Plynom / reelle / kompl. Potenz}\\}{g[n]=g_p[n]\quad/\quad g_r[n]\quad/\quad g_k[n]}\ne0\qquad→\qquad\overset{\text{gesucht}\\}{y_s[n]}$

| Polynom                                                      | Reelle Potenz                                           | Komplexe Potenz    ( $e^{j\frac2\pi}=j$ )                    |
| ------------------------------------------------------------ | ------------------------------------------------------- | ------------------------------------------------------------ |
| $g_p[n]=..+..n+..n^2$                                        | $g_r[n]=c_0·a^n$                                        | $g_k[n]=a~\cos[\omega~n] +b~\sin[\omega~n]$                  |
| $y_s[n]=an^2+bn+c\\y_s[n+1]=a(n+1)^2\\\qquad\qquad +b(n+1)\\\qquad\qquad+c$ | $y_s[n]=\frac1{d_2z^2+d_1z+d_0}\bigg\vert_{z=a}c_0~a^n$ | $\underline y_s=\frac1{d_2z^2+d_1z+d_0}\bigg \vert_{z=e^{j\omega}}·(a-bj)\\~\\\qquad\qquad\quad\qquad\qquad●—\!○\quad y_s[n]$ |
| y in 1. einsetzen und<br />nach a, b, c auflösen             | Wenn Ansatz fehlschlägt: *n                             | $g_k[n]=\boldsymbol{a^n}\big(a_0 \cos[\frac2\pi n] +b_0\sin[\frac2\pi n]\big)$ |
| y-Funktion ist Polynom n-ten Grades                          | Konstante ist  $c_0·1^n$                                | $\underline y_s=\frac1{d_2z^2+d_1z+d_0}\bigg \vert_{z=\boldsymbol{a}j}·\boldsymbol{a^n}(a_0-b_0j)$ |



<div style="page-break-after: always;"></div>

# 5. Numerisch



## 5.1 Differenzieren

| Vorwärts-Differenzen             | Zentrale Differenzen                  | Rückwärts-Differenzen            |
| :------------------------------- | ------------------------------------- | -------------------------------- |
| $\dot x[i]=\dfrac{x[i+1]-x[i]}h$ | $\dot x[i]=\dfrac{x[i+1]-x[i-1]}{2h}$ | $\dot x[i]=\dfrac{x[i]-x[i-1]}h$ |
| Vorbereitung für Zentral         | $t=i·h$                               | Nicht zu gebrauchen              |

bei  $\ddot x$	→	$\dot x=y\quad \ddot x=\dot y$ 



## 5.2 Runge-Kutta

$\dot x=f(x,y),\qquad \dot y=g(x,y) \qquad x(...)=x_0\qquad y(...)=y_0\qquad f'(x)=\frac{16}{15}N(\frac h2)-\frac 1{15}N(h)$

| t              | x                                   | y                                   | h · x'            | h · y'             |
| -------------- | ----------------------------------- | ----------------------------------- | ----------------- | ------------------ |
| $t_0$          | $x_0$                               | $y_0$                               | $f(x,y)\quad=f_1$ | $g(x,y)\quad =g_1$ |
| $t_0+\frac h2$ | $x_0+\frac{f_1}2$                   | $y_0+\frac{g_1}2$                   | $f(x,y)\quad=f_2$ | $g(x,y)\quad =g_2$ |
| $t_0+\frac h2$ | $x_0+\frac{f_2}2$                   | $y_0+\frac{g_2}2$                   | $f(x,y)\quad=f_3$ | $g(x,y)\quad =g_3$ |
| $t_0+h$        | $x_0+f_3$                           | $y_0+g_3$                           | $f(x,y)\quad=f_4$ | $g(x,y)\quad =g_4$ |
|                |                                     |                                     |                   |                    |
| $t_0+h$        | $x_1=x_0+\frac{f_1+2f_2+2f_3+f_4}6$ | $y_1=y_0+\frac{g_1+2g_2+2g_3+g_4}6$ |                   |                    |



<div style="page-break-after: always;"></div>

# 6. Jordansche Normalform



**Muster / Schablone**

| Matrix B            | HV 1. Stufe (= EV)    | HV 2. Stufe               | HV 3. Stufe                |
| ------------------- | --------------------- | ------------------------- | -------------------------- |
| $B_i=A-\lambda_i I$ | $B_i·\vec e_i=\vec 0$ | ${B_i}^2·\vec h_i=\vec 0$ | ${B_i}^3·\vec h_i'=\vec 0$ |



| EW / charakt. Polynom                                        |                Hamilton-Cayley / Min. Polynom                |                                           Jordan-Normal-Form |
| :----------------------------------------------------------- | :----------------------------------------------------------: | -----------------------------------------------------------: |
| **1. Beispiel**<br />$~\\\quad\lambda_1=\lambda_1=\lambda_1\quad\ne\quad\lambda_2$ | $B_1~\underbrace{B_1~\underbrace{B_1\underbrace{B_2}_{\vec h_1'}}_{\vec h_1}}_{\vec e_1}=B_2~\underbrace{B_1~B_1~B_1}_{\vec e_2}=\vec 0$ | $J=\begin{pmatrix}\lambda_1&1&&\\ &\lambda_1&1&\\&&\lambda_1&\\ &&&\lambda_2\end{pmatrix}$ |
| $\chi_A(z)=(z-\lambda_1)^3(z-\lambda_2)$                     |            $m_A(z)=(z-\lambda_1)^3(z-\lambda_2)$             | $S=\bigg(\vec e_1\quad \vec h_1\quad \vec h_1'\quad \vec e_2\bigg)$ |
| **2. Beispiel**<br />$~\\\quad\lambda_1=\lambda_1=\lambda_1\quad\ne\quad\lambda_2$ | $B_1~\underbrace{B_1\underbrace{B_2}_{\vec h_1}}_{\vec e_1}=B_2~\underbrace{B_1~B_1}_{\vec e_2}=\vec 0$ | $J=\begin{pmatrix}\lambda_1&&&\\ &\lambda_1&1&\\&&\lambda_1&\\ &&&\lambda_2\end{pmatrix}$ |
| $\chi_A(z)=(z-\lambda_1)^3(z-\lambda_2)$                     |            $m_A(z)=(z-\lambda_1)^2(z-\lambda_2)$             | $S=\bigg(\vec e_{1\alpha}\quad \vec e_{1}\quad \vec h_1\quad \vec e_2\bigg)$ |
| **3. Beispiel**<br />$~\\\quad\lambda_1=\lambda_1\quad\ne\quad\lambda_2=\lambda_2$ | $B_1~\underbrace{B_1\underbrace{B_2}_{\vec h_1}}_{\vec e_1}=B_2~\underbrace{B_1~B_1}_{\vec e_2}=\vec 0$ | $J=\begin{pmatrix}\lambda_1&1&&\\ &\lambda_1&&\\&&\lambda_2&\\ &&&\lambda_2\end{pmatrix}$ |
| $\chi_A(z)=(z-\lambda_1)^2(z-\lambda_2)^2$                   |            $m_A(z)=(z-\lambda_1)^2(z-\lambda_2)$             | $S=\bigg(\vec e_1\quad \vec h_1\quad \vec e_{2\alpha}\quad \vec e_{2\beta}\bigg)$ |
| *Potenz  =  Anzahl EW<br />            =  Algebraische Vielfachheit* | *Potenz  =  Anzahl HV-Stufen<br />                   =  Alg. VF – Geo. VF + 1* |          *Anzahl 1en = Anzahl HVen<br />= Alg. VF – Geo. VF* |

*Algebraische Vielfachheit = Anzahl Eigenwerte*

*Geometrische Vielfachheit = Anzahl Eigenvektoren  (genau: Anzahl voneinander linar unabhängiger EV)* 

<div style="page-break-after: always;"></div>



---

# ———————  S I G N A L E  ————————

https://www.youtube.com/watch?v=hVOA8VtKLgk



# 1. [Komplexe Zahlen](https://www.youtube.com/watch?v=LxPUwlQ2wn0) ([Englisch](https://www.youtube.com/watch?v=T647CGsuOVU))

$\boxed{\quad\text{Gleiche Vorzeichen:}\quad\begin{matrix}+j·+j=-1\\-j·-j=-1\end{matrix}\qquad\qquad\text{Unterschiedliche Vorzeichen:}\quad\begin{matrix}+j·-j=1\\-j·+j=1\end{matrix}\quad}$

| [Kartesisch](https://youtu.be/TSeC_2D8xNs)              | Polar                                                        |
| ------------------------------------------------------- | ------------------------------------------------------------ |
| $z=\begin{pmatrix}x\\y\end{pmatrix}=x+yj$               | $z=r·\underline{\big/\varphi}=r·e^{j\varphi}$                |
| $Re(z)=x=r·\cos(\varphi)\\\ \\ Im(z)=y=r·\sin(\varphi)$ | $\vert z\vert=r=\sqrt{x^2+y^2}\qquad arg(z)=\varphi=\bigg\{\begin{matrix}\arctan(\frac yx)&x>0\\\pm90°&x=0\\\arctan(\frac yx)\pm180°&x<0\end{matrix}$ |

[**Addition / Subtraktion / Multiplikation / Division**](https://www.youtube.com/watch?v=zB2VwWzpYx4)

|                   | Allgemein                                                    | [Multiplikation](https://youtu.be/zB2VwWzpYx4?t=395)         | [Division](https://youtu.be/zB2VwWzpYx4?t=684)               |
| ----------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <u>Polar</u>      | $z_1=r_1·\underline{\big/\varphi_1}\\ z_2=r_2·\underline{\big/\varphi_2}$ | $z_1·z_2=r_1·r_2\ \underline{\big/\varphi_1+\varphi_2}$      | $\dfrac{z_1}{z_2}=\dfrac{r_1}{r_2}·\underline{\big/\varphi_1-\varphi_2}$ |
| <u>Kartesisch</u> | $z_1=x_1+y_1j\\z_2=x_2+y_2j$                                 | $z_1·z_2=(x_1x_2-y_1y_2)\\\qquad\qquad\qquad+(x_1y_2+x_2y_1)j$ | $\dfrac{z_1}{z_2}=\dfrac{z_1·{z_2}^*}{z_2·{z_2}^*}=\dfrac{z_1·{z_2}^*}{\vert z_2\vert^2}$ |

| [Potenz](https://youtu.be/G_FRNyHpzrk)                       | [Wurzel](https://youtu.be/BKdqTn2iO4s)                       | [Konjugieren](https://youtu.be/zB2VwWzpYx4?t=242) | Extra                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------- | --------------------------------------------------------- |
| $z^n=r^n·\underline{\big/ \varphi~n}\\~\\\quad=r^n·e^{j\varphi ~n}$ | $z_k=\sqrt[n]{r}·\underline{\big/\frac\varphi n+k·\frac{360°}n}\\~\\ k=0,..,n–1$ | $z^*=x-jy\\~\\\quad=r·\underline{\big/-\varphi}$  | $e^{2\pi j}=1\qquad\dfrac 1z=\dfrac{z^*}{\vert z\vert^2}$ |

| [Eulersche Formel](https://youtu.be/TGJHnQY9cjA) | e-Funktion                                    | Trigos                                                       |
| ------------------------------------------------ | --------------------------------------------- | ------------------------------------------------------------ |
| $$e^{x+jy}=e^x·(\cos(y)+j\ \sin(y))$$            | $e^{j\varphi}=\cos(\varphi)+j\ \sin(\varphi)$ | $\bold2\cos(\omega_0t)=e^{j\omega_0t}\boldsymbol+e^{-j\omega_0t}\\\bold{2j}\sin(\omega_0t)=e^{j\omega_0t}\boldsymbol-e^{-j\omega_0t}$ |



<div style="page-break-after: always;"></div>

# 2. Periodische Funktion



## 2.1 [Zeiger](https://shortmath.com/transforming-acos%E2%81%A1xbsin%E2%81%A1x-into-rcos%E2%81%A1x-%CE%B1/)

|                        x, y  ≙  a, b                         |                           Beispiel                           |                          Allgemein                           |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| <img src="./MA2%20Zusammenfassung/image-20200505145609699.png" alt="image-20200505145609699" style="zoom: 22%;" /> | <img src="./MA2%20Zusammenfassung/image-20200611133649402_gedreht.png" alt="image-20200611133649402_gedreht" style="zoom: 33%;" /> $\begin{matrix}1-2j&=&\sqrt5·\underline{/-63°}\\~\\ 1\cos(t)+2\sin(t)&=&\sqrt5·\cos(t-63°)\end{matrix}$ | <img src="./MA2%20Zusammenfassung/image-20200611133649402_gedreht.png" alt="image-20200611133649402_gedreht" style="zoom: 33%;" /> $\begin{matrix}x+yj&=&r·\underline{/\varphi}\\~\\ a\cos(t)+b\sin(t)&=&r·\cos(t+\varphi)\end{matrix}$ |



$\sin(\omega t)=\cos(\omega t-90°)$



| Differenzieren                                               | Integrieren                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="./MA2%20Zusammenfassung/image-20200505152713744.png" alt="image-20200505152713744" style="zoom:33%;" /> | <img src="./MA2%20Zusammenfassung/image-20200505153227677.png" alt="image-20200505153227677" style="zoom:33%;" /> |
| $\frac d{dt}\cos(\omega t)=-\omega\sin(\omega t)=\omega\cos(\omega t+\frac\pi2)$ | $\int \cos(\omega t)dt=\frac 1\omega\sin(\omega t)$          |
| Ableiten:  $·j\omega$                                        | Integrieren:  $·\dfrac1{j\omega}$                            |

Schwingung / realer Vorgang  <img src="./MA2%20Zusammenfassung/image-20200505152923990.png" alt="image-20200505152923990" style="zoom:33%;" />  Modellierung durch Zeiger



<div style="page-break-after: always;"></div>

## 2.2  a) Spektraldarstellung (Reell)

$\quad x(t)=\underbrace{\dfrac{a_0}2}_{Gleichanteil}+\underbrace{(a_1\cos(1~\omega_0 t)+b_1\sin(1~\omega_0 t))}_{x_1(t):\quad1.\ Oberschwingung}+\underbrace{(a_2\cos(2~\omega_0 t)+ b_2\sin(2~\omega_0 t))}_{x_2(t):\quad 2.\ Oberschwingung}+~...$

| $\omega$     | kartesisch     | polar                              |
| ------------ | -------------- | ---------------------------------- |
| $0$          | $\dfrac{a_0}2$ | $\dfrac{a_0}2·\underline{\big/0°}$ |
| $1~\omega_0$ | $a_1-b_1j$     | $r_1·\underline{\big/\varphi_1}$   |
| $2~\omega_0$ | $a_2-b_2j$     | $r_2·\underline{\big/\varphi_2}$   |

|                 reelles Amplituden-Spektrum                  |                   reelles Phasen-Spektrum                    |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| <img src="../../Typora.assets/DSC_0524.JPG" alt="DSC_0524" style="zoom: 10%;" /> | <img src="../../Typora.assets/DSC_0527.JPG" alt="DSC_0527" style="zoom: 11%;" /> |



## 2.2  b) Spektraldarstellung (Komplex)

$\quad x(t)=\dfrac{a_0}2+\left(\dfrac{a_1}2-\dfrac{b_1}2\right)e^{1·j\omega_0t}+\left(\dfrac{a_2}2-\dfrac{b_2}2\right)e^{2·j\omega_0t}+~...\\~\\\qquad\qquad\quad+\left(\dfrac{a_1}2+\dfrac{b_1}2\right)e^{-1·j\omega_0t}+\left(\dfrac{a_2}2+\dfrac{b_2}2\right)e^{-2·j\omega_0t}+~...$

| $\omega$0                          | kartesich                      | polar                                                        |
| ---------------------------------- | ------------------------------ | ------------------------------------------------------------ |
| $0$                                | $\dfrac{a_0}2$                 | $\dfrac{a_0}2·\underline{\big/0°}$                           |
| $1~\omega_0\quad/\quad-1~\omega_0$ | $a_1-b_1j\quad/\quad a_1+b_1j$ | $\dfrac{r_1}2·\underline{\big/\varphi_1}\qquad/\qquad \dfrac{r_1}2·\underline{\big/-\varphi_1}$ |
| $2~\omega_0\quad/\quad-2~\omega_0$ | $a_2-b_2j\quad/\quad a_2+b_2j$ | $\dfrac{r_2}2·\underline{\big/\varphi_2}\qquad/\qquad \dfrac{r_2}2·\underline{\big/-\varphi_2}$ |

|                komplexes Amplituden-Spektrum                 |                  komplexes Phasen-Spektrum                   |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| <img src="../../Typora.assets/DSC_0528.JPG" alt="DSC_0528" style="zoom:10%;" /> | <img src="../../Typora.assets/DSC_0531.JPG" alt="DSC_0531" style="zoom:10%;" /> |
|            gerade / achsensymmetrisch zu y-Achse             |           ungerade / punktsymmetrisch zu Ursprung            |

<div style="page-break-after: always;"></div>

## 2.3 Fourier-Reihe

| Reelle Darstellung                                           | Komplexe Darstellung                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\displaystyle x(t)=\dfrac{a_0}2+\sum_{k=1}^\infty~a_k\cos(k~\omega_0t)+b_k\sin(k~\omega_0t)$ | $\displaystyle x(t)=\sum_{n=-\infty}^\infty~c_n·e^{j~k~\omega_0t}$ |
| $\displaystyle a_0=\dfrac2p\int_{gesamte\\Periode}x(t)~dt$   | $\displaystyle c_k=\frac1p\int_{gesamte\\Periode} x(t)\ e^{-jk\frac{2\pi}pt}dt$ |
| $a_0=2c_0\qquad a_k=2·\Re\{c_k\}\qquad b_k=-2j·\Im\{c_k\}$   | $c_0=\dfrac{a_0}2\qquad c_k=\dfrac{a_k-b_kj}2\qquad c_{-k}=\dfrac{a_k+b_kj}2$ |



## 2.4 Sprungstellenanalyse

Funktion f + Ableitungen besitzt innerhalb Periode p die Sprungstellen  $t_0,..,t_n$  mit Sprunghöhen <img src="./MA2%20Zusammenfassung/image-20200513075829534.png" alt="image-20200513075829534" style="zoom:60%;" /> 	<img src="../../Typora.assets/image-20200513075616066.png" alt="image-20200513075616066" style="zoom:50%;" />

$D_0:$	Änderung der y-Werte (Sprung)		$D_1:$	Änderung der Steigung (1. Ableitung)

---

<img src="./MA2%20Zusammenfassung/image-20200513075444973.png" alt="image-20200513075444973" style="zoom: 50%;" />  	<img src="../../Typora.assets/image-20200513075458034.png" alt="image-20200513075458034" style="zoom: 50%;" />

![image-20200513075426677](./MA2%20Zusammenfassung/image-20200513075426677.png)



<div style="page-break-after: always;"></div>

# 3. [Fourrier-](https://www.youtube.com/watch?v=spUNpyF58BY&t=991s) / 4. Laplace- / 5. [Z-](https://youtu.be/4IC2Lg9yFoo)Transformation

 


**Fourrier-Transformation**			    		**Laplace-Transformation**	   	 **Inverse Fourrier-Transformation**
$$
\check x(\omega)=\int_{-\infty}^\infty x(t) \ e^{-j\omega t}\ dt\qquad\qquad\qquad \check x(s)=\int_0^\infty x(t)\ e^{-st}\ \text{dt}\qquad\qquad\qquad x(t)=\dfrac 1{2\pi}\int_{-\infty}^\infty \check x(\omega)\ e^{j\omega t}\ d\omega
$$



**Zweiseitige Z-Transformation**			**einseitige Z-Transformation**			  	**Inverse Z-Transformation**
$$
X(z)=\sum_{i=-\infty}^{\infty}x[i]\ z^{-i}\qquad\quad\qquad\qquad X^+(z)=\sum_{n=0}^{\infty}x[n]\ z^{-n}\qquad\qquad\qquad x[n]=\dfrac1{2\pi j}\ \oint_K X(z)\ z^{n-1}\ dz
$$

 


## [Sätze](https://link.springer.com/book/10.1007/978-3-8348-9292-8)

|                Fourrier                | Laplace    (immer  ==· ε(t)== )  | Z    (immer  ==· ε[n]== )  |
| :------------------------------------: | :------------------------------: | :------------------------: |
| $x(t)\quad ○—\!●\quad\check x(\omega)$ | $x(t)\quad○—\!●\quad\check x(s)$ | $x[n]\quad○—\!●\quad X(z)$ |

**Verschiebung / Dämpfung**

| $x(t-t_0)\quad ○—\!●\quad e^{-j\omega t_0}\ \check x(\omega)$ | $x(t-a)\quad○—\!●\quad e^{-as}\ \check x(s)$ |   $x[n-k]\quad○—\!●\quad z^{-k}\ X(z)$ |
| ------------------------------------------------------------ | :------------------------------------------: | -------------------------------------: |
| $\check x(\omega-\omega_0)\quad●—\!○\quad e^{j\omega_0t}\ x(t)$ | $\check x(s+a)\quad●—\!○\quad e^{-at}\ x(t)$ | $a^n\ x[n]\quad○—\!●\quad X(\frac za)$ |

**Skalierung / Spiegelung**

| $x(at)\quad○—\!●\quad \dfrac1{\vert a\vert}\ \check x(\frac\omega a)$ | $x(at)\quad○—\!●\quad\dfrac 1a\ \check x(\frac sa)$ | $a^n\ x[n]\quad○—\!●\quad X(\frac za)$ |
| ------------------------------------------------------------ | :-------------------------------------------------: | -------------------------------------- |
| $x(-t)\quad ○—\!●\quad \check x(-\omega)=\check x(\omega)^*$ |                                                     | $x[-n]\quad○—\!●\quad X(\frac1z)$      |

**Differentiation / Integration**

| $\dot x(t)\quad ○—\!●\quad j\omega·\check x(\omega)$         | $\dot x(t)\quad○—\!●\quad s\ \check x(s)-x(0)\\~\\\ddot x(t)\quad○—\!●\quad s^2\check x(s)-s\ x(0)\\\qquad\qquad\qquad\qquad\quad-\dot x(0)\\~\\\dddot x(t)\quad○—\!●\quad s^3\check x(s)-s^2x(0)\\\qquad\qquad\qquad-s\ \dot x(0)-\ddot x(0)$ | $x[n]\quad○—\!●\quad X^+(z)\\~\\x[n+1]~~○—\!●~~z\ X^+(z)-z\ x[0]\\~\\x[n+2]\quad○—\!●\quad z^2X^+(z)\\\qquad\qquad\qquad~-z^2x[0]-z\ x[1]$ |
| ------------------------------------------------------------ | :----------------------------------------------------------: | :----------------------------------------------------------: |
| $X(t)\quad ○—\!●\quad \frac1{j\omega}·\check x(\omega)\\\quad\qquad ~~~+\pi~\check x(0)~\delta(\omega)$ |          $X(t)\quad○—\!●\quad \dfrac1s·\check x(s)$          |                                                              |
| $\check x'(\omega)\quad ●—\!○\quad -t\ x(t)$                 |            $\check x'(s)\quad●—\!○\quad-t\ x(t)$             |                $X'[z]~~●—\!○~~-(n-1)\ x[n-1]$                |

<div style="page-break-after: always;"></div>

**Funktions-Paare**	(a > 0)

| $\qquad\varepsilon(t) \quad ○—\!●\quad \dfrac1{j\omega}\\~\\{e^{-at}·\varepsilon(t)}\quad ○—\!●\quad \dfrac1{j\omega+a}$ | $\qquad\varepsilon(t)\quad○—\!●\quad\dfrac 1s\\~\\e^{-at}·\varepsilon(t)\quad○—\!●\quad\dfrac1{s+a}$ | $\varepsilon[n]\quad○—\!●\quad \dfrac z{z-1}\\a^n\varepsilon[n]\quad○—\!●\quad\dfrac z{z-a}\\a^n\varepsilon[-n]\quad○—\!●\quad \dfrac a{a-z}$ |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------- |
| $\qquad t·\varepsilon(t) \quad ○—\!●\quad \dfrac1{(j\omega)^2}\\~\\e^{-at}·t·\varepsilon(t) \quad ○—\!●\quad \dfrac1{(j\omega+a)^2}$ | $\qquad t·\varepsilon(t)\quad○—\!●\quad\dfrac1{s^2}\\~\\e^{-at}·t·\varepsilon(t)\quad○—\!●\quad\dfrac1{(s+a)^2}$ | $n~\varepsilon[n]\quad○—\!●\quad \dfrac z{(z-1)^2}\\~\\a^n~n~\varepsilon[n]\quad○—\!●\quad \dfrac{az}{(z-a)^2}$ |
| $\dfrac{t^2}2·\varepsilon(t) \quad ○—\!●\quad \dfrac1{(j\omega)^3}$ | $\dfrac{t^n}{n!}·\varepsilon(t)\quad○—\!●\quad \dfrac1{s^{n+1}}$ | $\frac{(n-1)(n-2)}2\ \varepsilon[n-1]\\○—\!●\quad \frac 1{(z-1)^{3}}$ |
|       $rect(\frac tT)\ ○—\!●\ T\ si(\frac{\omega T}2)$       |                                                              | $a^{n-1}\ \varepsilon[{\scriptstyle n-1}]~~○—\!●~~\dfrac1{z-a}$ |
| $\delta(t)\quad ○—\!●\quad 1\\1\quad○—\!●\quad2\pi\ \delta(\omega)$ | $\quad~\delta(t)\quad ○—\!●\quad 1\\a·\delta(t)\quad ○—\!●\quad a$ | $\quad \delta[n]\quad○—\!●\quad 1\\\delta[n-k]\quad○—\!●\quad z^{-k}$ |
| ${Ш_T(t)\quad○—\!●\quad\dfrac{2\pi}T·Ш_{\frac{2\pi}T}(\omega)}$ |                                                              |                                                              |
| $\cos(\omega_0t)\quad○—\!●\quad\pi~\delta (\omega+\omega_0)\\\qquad\qquad\qquad+\pi~\delta (\omega-\omega_0)$ | $\qquad\cos(\omega_0t)\quad○—\!●\quad\dfrac s{s^2+{\omega_0}^2}\\~\\e^{-at}\cos(\omega_0t)\quad○—\!●\quad\frac{s+a}{(s+a)^2+{\omega_0}^2}$ |                                                              |
| $\sin(\omega_0t)\quad○—\!●\quad j\pi~\delta(\omega+\omega_0)\\\qquad\qquad\qquad-j\pi~\delta(\omega-\omega_0)$ | $\qquad\sin(\omega_0t)\quad○—\!●\quad\dfrac{\omega_0}{s^2+{\omega_0}^2}\\~\\e^{-at}\sin(\omega_0t)\quad○—\!●\quad\frac{\omega_0}{(s+a)^2+{\omega_0}^2}$ |                                                              |
| $\check x(t)\big/\big(\check x(jt)\big)\quad ○—\!●\quad 2\pi\ x(-\omega)$ |                                                              |                                                              |

**Faltungssatz**

| $x(t)\circledast h(t)\quad○—\!●\quad \int_{-\infty}^\infty x(\tau)h(t-\tau)\ d\tau$ | $x[n]\circledast h[n]\quad○—\!●\quad\sum_{i=0}^n x[i]·h[n-i]$ |
| ------------------------------------------------------------ | :----------------------------------------------------------: |
| $x(t)·h(t)\quad○—\!●\quad \dfrac1{2\pi}\ \check x(\omega)\circledast\check h(\omega)$ |                                                              |

<div style="page-break-after: always;"></div>

## 3. Fourrier

| Si-Funktion           ==evtl. noch Zeichnung sin/cos gemondet== |
| ------------------------------------------------------------ |
| $\dfrac{\sin(\omega t)}{\omega t}=si(\omega t)\\\ \\c(k\frac{2\pi}p)=\dfrac 1p si(\frac{k\pi}p)$                        <img src="./MA2%20Zusammenfassung/image-20200522164043838.png" alt="image-20200522164043838" style="zoom: 28%;" /> |



## 4. Laplace

<img src="../../Typora.assets/image-20200525130012482.png" width=75% />

| Partialbruch-zerlegung | Nenner in LF zerlegen                                        | Gleichung · HN                                             |                                                              |
| ---------------------- | ------------------------------------------------------------ | ---------------------------------------------------------- | ------------------------------------------------------------ |
| einfache LF            | $\frac{ax+b}{x^2+cx+d}=\frac{ax+b}{(x-x_1)(x-x_2)}\\=\frac A{(x-x_1)}+\frac B{(x-x_2)}$ | $ax+b=A(x-x_2)\\\qquad\qquad+B(x-x_1)$                     | $A=\frac{ax+b}{(x-x_2)}\bigg\vert_{x=x_1}\\B=\frac{ax+b}{(x-x_1)}\bigg\vert_{x=x_2}$ |
| mehrfache LF           | $\frac{u(x)}{v(x)}=\frac{u(x)}{(x-x_1)(x-x_2)^2}\\=\frac A{x-x_1}+\frac B{x-x_2}+\frac C{(x-x_2)^2}$ | $u(x)=A(x-x_2)^2\\\qquad+B(x-x_1)(x-x_2)\\\qquad+C(x-x_2)$ | A & C wie oben, B mit x-Wert                                 |
| irreelle Nullstellen   | $\frac{u(x)}{v(x)}=\frac{u(x)}{(x-x_1)(x^2+x_2)}\\=\frac A{x-x_1}+\frac {Bx+C}{x^2+x_2}$ | $u(x)=A(x^2+x_2)\\\qquad+(Bx+C)(x-x_1)$                    | Koeffizeinten-vergleich                                      |

==**Komplexe Nullstelle:**==	$s_{12}=x\pm yj\quad →\quad(s-x)^2+y^2\overset!=0$



## 5.   — Z —

| Komplex $\quad a=x+jy=r\underline{/\varphi}$                 |
| ------------------------------------------------------------ |
| $\dfrac z{(z-a)(z-a^*)}=\dfrac z{z^2-2az+(x^2+y^2)}\quad●—\!○\quad \dfrac 1y\ r^n \sin\big(n\ \varphi\big)\ \varepsilon[n]$ |
| $\dfrac {c_0}{(z-a)(z-a^*)}=\dfrac{c_0}{z^2-2az+(x^2+y^2)}\quad●—\!○\quad \dfrac{c_0}y\ r^{n-1} \sin\big((n-1)\varphi\big)\ \varepsilon[n-1]$ |

<div style="page-break-after: always;"></div>

# 6. [Diskrete Fourrier-Transformation](https://youtu.be/g8RkArhtCc4?t=773)


​																			**DFT**													**IDFT**
$$
\alpha=e^{j\frac{2\pi}N}\qquad\qquad\check x[k]=X(\alpha^k)\qquad\qquad\qquad\qquad x[n]=\dfrac1N\check X(\alpha^{-n})\qquad\\\ \\n=4:\qquad\begin{pmatrix}x[0]\\x[1]\\x[2]\\x[n]\end{pmatrix}\qquad\qquad○—\!●\qquad\qquad\begin{pmatrix}\check x[0]\\\check x[1]\\\check x[2]\\\check x[k]\end{pmatrix}
$$


| a)   alpha zeichnen  | <img src="./MA2%20Zusammenfassung/image-20200920105858274-1678812658194-4.png" alt="image-20200920105858274" style="zoom: 50%;" />   $\ →\\○—\!●$ | <img src="./MA2%20Zusammenfassung/image-20200615095511482-1678812658195-5.png" alt="image-20200615095511482" style="zoom:33%;" />   $\,\,←\\○—\!●$ |
| -------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **b)   Z-Transform** | $X(z)=x[0]\ z^{-0}+x[1]\ z^{-1}+x[2]\ z^{-2}$                | $\check X(z)=\check x[0]\ z^{-0}+\check x[1]\ z^{-1}+\check x[2]\ z^{-2}$ |
| **c)   Einsetzen**   | $\begin{matrix}\check x[0]=X(\alpha^0)\\\check x[1]=X(\alpha^1)\end{matrix}\qquad \begin{matrix}\check x[2]=X(\alpha^2)\\\check x[k]=X(\alpha^k)\end{matrix}$ | $\begin{matrix}x[0]=X(\alpha^{-0})\\ x[1]=X(\alpha^{-1})\end{matrix}\qquad \begin{matrix}x[2]=X(\alpha^{-2})\\ x[n]=X(\alpha^{-n})\end{matrix}$ |



| N=6    $(60°)$                        | N=8    $(45°)$                        | N=10    $(36°)$                                              |
| ------------------------------------- | ------------------------------------- | ------------------------------------------------------------ |
| $\alpha^{-5}=\alpha^1=0.500+0.866\ j$ | $\alpha^{-7}=\alpha^1=0.707+0.707\ j$ | $\alpha^{-9}=\alpha^1=0.809+0.588\ j\\\alpha^{-8}=\alpha^2=0.309+0.951\ j$ |

|                    | "rein"                         | Fourrierreihe<br />(k. Oberschwingung) | Fourier-Trafo          | Allgemein       |
| ------------------ | ------------------------------ | :------------------------------------- | ---------------------- | --------------- |
| **Vorfaktor DFT**  | $1$                            | $c_k=\dfrac1N·\check x[k]$             | $\Delta t$             | $c$             |
| **Vorfaktor IDFT** | $x[n]=\dfrac1N·X(\alpha^{-n})$ | $1$                                    | $\dfrac1{N\ \Delta t}$ | $\dfrac1{N\ c}$ |



<div style="page-break-after: always;"></div>

##  6.6 Fast Fourier Transform

**N = n~a~ ·  n~b~ · n~c~**				$~\\\displaystyle\hat x=\sum_{c=0}^{n_c-1}\ \left(\ \sum_{b=0}^{n_b-1}\ \bigg(\ \sum_{a=0}^{n_a-1}\ x\big[(n_c n_b)\ a+(n_c)\ b+c\big]\ \overrightarrow{\alpha^{(n_cn_b)a}}\ \bigg)\ \overrightarrow{\alpha^{(n_c)b}}\ \right)\ \overrightarrow{\alpha^{c}}\\$


| Positionsformel          | Umrechnung                                | Anzahl Butterflies                             |
| ------------------------ | ----------------------------------------- | ---------------------------------------------- |
| $c\ (n_bn_a)+b\ (n_a)+a$ | $\big[(n_c n_b)\ a_0+(n_c)\ b_0+c_0\big]$ | $\#=\dfrac N{n_a}+\dfrac N{n_b}+\dfrac N{n_c}$ |
| c mal was dahinter kommt | $c_0\ (n_bn_a)+b_0\ (n_a)+a_0$            |                                                |



<img src="./MA2%20Zusammenfassung/image-20200620124607649.png" alt="image-20200620124607649" style="zoom:40%;" />  



<div style="page-break-after: always;"></div>

# 7. Analoges - Diskretes Spektrum



## 7.0 Nyquist

$\dfrac{2\pi}{\Delta t}>2~\omega_{max}\qquad \Delta t<\dfrac{2\pi}{2\omega}=\dfrac\pi\omega$



## 7.1 Dirac-Kamm

| Multiplikation                                               | Faltung  (T=3)                                               |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20200715080752265](./MA2%20Zusammenfassung/image-20200715080752265.png) | ![image-20200715080813129](./MA2%20Zusammenfassung/image-20200715080813129.png) |
| $x_{\Delta t}(t)=x(t)·Ш_{\Delta t}(t)\\\ \\○—\!●\\\check x_{\Delta t}(\omega)=\dfrac1{\Delta t}\check x(\omega)\circledastШ_{\frac{2\pi}{\Delta t}}(\omega)$ | $x(t)\circledastШ_T(t)\quad○—\!●\quad \check x(\omega)·\frac{2\pi}T~Ш_{\frac{2\pi}T}(\omega)$ |



## 7.6 Bandpass - Tiefpass

<img src="./MA2%20Zusammenfassung/image-20200715195610160.png" alt="image-20200715195610160" style="zoom: 50%;" />

| In-Phase Komponente                                          | Quadratur Komponente                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="./MA2%20Zusammenfassung/image-20200715201657664.png" alt="image-20200715201657664" style="zoom: 45%;" /> | <img src="./MA2%20Zusammenfassung/image-20200715201716372.png" alt="image-20200715201716372" style="zoom: 45%;" /> |
| $x(t)*2\cos(\omega_0t)\quad○—\!●\\\ \\\check x(\omega)\circledast\big[\delta(\omega+\omega_0)+\delta(\omega-\omega_0)\big]\quad=\check x_I(\omega)$ | $x(t)*\big(-2\sin(\omega_0t)\big)\quad○—\!●\\\ \\x(t)\circledast j\ \big[-\delta(\omega+\omega_0)+\delta(\omega-\omega_0)\big]\quad=\check x_Q(\omega)$ |

<div style="page-break-after: always;"></div>

# 9. Zufallssignale


$$
\phi_{xx}(t)\quad○—\!●\quad S_{xx}(\omega)=\vert \check x(\omega)\vert^2=\check x(\omega)^*·\check x(\omega)\\\ \\\phi_{yy}=\phi_{hh}^E\circledast\phi_{xx}\quad○—\!●\quad S_{yy}=\vert\check h\vert^2·S_{xx}=\vert\ddot U\vert^2·S_{xx}
$$



**Gegeben:**		h[n]:  1  -1		Φ~xx~[n]:  1  2  4  2  1

```
h[-n]*h[n]	-1  1 · 1 -1			Φxx[n]	1  2  4  2  1 · -1  2 -1	Φhh[n]
			————————————					————————————————————————
				-1  1							-1 -2 -4 -2 -1
					1  -1							2  4  8  4  2
			—————————————							  -1 -2 -4 -2 -1
	Φhh[n]		-1	2  -1					————————————————————————
									Φyy[n]		-1  0 -1  4 -1  0 -1
											   (-3)		 (0)	  (3)
```

$\phi_{yy}\quad○—\!●\quad S_{yy}=4-1\big(e^{-j\omega}+e^{j\omega}\big)-1\big(e^{-3j\omega}+e^{3j\omega}\big)=4-2\cos(\omega)-2\cos(3\omega)$



# B. Wavelets

<img src="../../Typora.assets/image-20200716174033669.png" alt="image-20200716174033669" style="zoom:45%;" />  

 





<div style="page-break-after: always;"></div>

---

# ———————  S Y S T E M E  ————————



# 1. 2. Stetige    /    3. Diskrete  Systeme

$$
~\\\qquad\qquad\qquad\bold{Stetig~(Laplace)}\qquad\qquad\qquad\qquad\qquad\qquad\bold{Diskret~(Z)}\\~\\\qquad\qquad\qquad\quad y(t)=h(t)\circledast x(t)\qquad\quad\qquad\qquad\qquad\qquad y[n]=h[n]\circledast x[n]
$$
​																		<img src="./MA2%20Zusammenfassung/image-20200611133649402.png" alt="image-20200611133649402" style="zoom: 33%;" />																				<img src="../../Typora.assets/image-20200611133649402.png" alt="image-20200611133649402" style="zoom: 33%;" />
$$
\qquad\qquad\qquad\quad\check y(s)=\ddot U(s)·\check x(s)\qquad\quad\qquad\qquad\qquad\qquad Y(z)=H(z)·X(z)\\~
$$

|  Gegeben: Übertragungsfunktion   |        $\ddot U(s)=\dfrac{\check y(s)}{\check x(s)}$         | $H(z)=\dfrac{Y(z)}{X(z)}$ |
| :------------------------------: | :----------------------------------------------------------: | ------------------------- |
| Möglichkeiten für  $x(t)~/~x[n]$ | $\overset{\text{Stoß/Impuls}\\}{x(t)=\delta(t)}\qquad\overset{\text{Sprung}\\}{x(t)=\varepsilon(t)}\\~\\\overset{\text{Eingangssignal: Funktion}\\}{x(t)=c·e^{at}\big/a\cos(\omega t)\big/b\sin(\omega t)}$ |                           |

 

## a) Differential- / Differenzen- Gleichung

| Gegeben:   | $\ddot U(s)=\dfrac{\check y(s)}{\check x(s)}=\dfrac{d~s+e}{a~s^2+b~s+c}$ |     $H(z)=\dfrac{Y(z)}{X(z)}=\dfrac{d~z+e}{a~z^2+b~z+c}$     |
| ---------- | :----------------------------------------------------------: | :----------------------------------------------------------: |
|            | Nener / Zähler: Vorfaktoren y / x<br />s-Potenz: Grad der Ableitung | Nener / Zähler: Vorfaktoren y / x<br />z-Potenz: Verschiebung von n |
| **D(z)gl** |            $a~\ddot y+b~\dot y+c~y=d~\dot x+e~x$             | $a~y[{\scriptstyle n+2}]+b~y[{\scriptstyle n+1}]+c~y[{\scriptstyle n}]=d~x[{\scriptstyle n+1}]+e~x[{\scriptstyle n}]$ |



## b) Frequenz- / Amplituden- / Phasengang

| Gegeben:                                 | $\ddot U(s)\qquad($Substitution:  $s=j\omega)$               | $H(z)\quad($Substitution:  $z=e^{j\omega})$                  |
| ---------------------------------------- | :----------------------------------------------------------- | ------------------------------------------------------------ |
| **Frequenzgang**                         | $\underline f(\omega)=\ddot U(j\omega)\qquad=\dfrac{a+jb}{c+jd}$ | $\underline f(\omega)=H(e^{j\omega})\quad = \dfrac1{Re+Im}$  |
| **Amplitudengang**<br />(Betrag komplex) | $\vert \underline f(\omega)\vert=\dfrac{\sqrt{a^2+b^2}}{\sqrt{c^2+d^2}}$ | $\vert \underline f(\omega)\vert =\dfrac1{\sqrt{Re^2+Im^2}}$ |
| **Phasengang**                           | $arg\!\left(\underline f(\omega)\right)=\arctan\left(\frac ba\right)-\arctan\left(\frac dc\right)$ | $arg\!\left(\underline f(\omega)\right)=\arctan\left(\dfrac{Im}{Re}\right)$ |

<div style="page-break-after: always;"></div>

## c) Stoß- / Sprung- Antwort

| Gegeben: Stoß              | $x(t)=\delta(t)\quad○—\!●\quad \check x(s)=1$                | $x[n]=\delta[n]\quad○—\!●\quad X(z)=1$                       |
| -------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Stoß-antwort**$~\\~\\$   | $\check y(s)=\ddot U(s)·\underbrace1_{\check x(s)}\qquad\overset{PBZ}{●—\!○}\qquad y(t)\\$ | $Y(z)=H(z)·\underbrace1_{X(z)}\qquad\overset{PBZ}{●—\!○}\qquad y[n]\\$ |
| **Gegeben: Sprung**        | $x(t)=\varepsilon(t)\quad○—\!●\quad\check x(s)=\dfrac1s$     | $x[n]=\varepsilon[n]\quad○—\!●\quad X(z)=\dfrac{z}{z-1}$     |
| **Sprung-antwort**$~\\~\\$ | $\check y(s)=\ddot U(s)·\underbrace{\dfrac1s}_{\check x(s)} \qquad\overset{PBZ}{●—\!○}\qquad y(t)$ | $Y(z)=H(z)·\underbrace{\dfrac z{z-1}}_{X(z)}\qquad\overset{PBZ}{●—\!○}\qquad y[n]$ |



## d) Eingang <u>stationär</u>

| Sprungantwort                   |                                                              | $\underline f(0)=H(e^{j0})=H(1)$                             |
| ------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Gegeben: Trigonom. Funktion** | $x(t)=a~\cos(\omega~t)+b~\sin(\omega~t)\\~\\○—\!●\quad \underline x=a-bj$ | $x[n]=a\ \cos[n\ \omega]+b\ \sin[n\ \omega]\\~\\○—\!●\quad \underline x=a-bj$ |
| **stationärer Ausgang**         | $\underline y=\ddot U(j\omega)·\underbrace{(a-bj)}_{\underline x}\quad○—\!●\quad y(t)$ | $\underline y=H(e^{j\omega})·\underbrace{(a-bj)}_{\underline x}\quad●—\!○\quad y[n]$ |



## e) Eingang <u>nicht stationär</u> ([Video SYS-13](https://0ytrr3zvx8hoqv2d.myfritz.net/nextcloud/index.php/s/K8eGYEo3noGKYdN?dir=undefined&path=%2FMathematik%2FMA2%20Aufgaben%20Videos))

<img src="../../Typora.assets/image-20221123100241170.png" width=91% />

<div style="page-break-after: always;"></div>

# 4. Faltung (Convolution)



## 4.1 Diskrete Faltung

$$
y[n]=x[n]\circledast h[n]\qquad→\qquad y[n]=\sum_{i=0}^n x[i]·h[n-i]
$$



<img src="../../Typora.assets/image-20200614083123564.png" alt="image-20200614083123564" style="zoom:28%;" />



**Multiplizieren**

$\quad h\circledast x$		h:	-1 0 1 2		x:	2 1 1							h:	2 1 0 -1		x:	$\overline{012}$

```
-1  0   1   2  ·  2   1   1				0	1	2	0	1	2  ·  2 1 0 -1
––––––––––––––––––––––––----			——————————————————————————————————
-2  0   2   4	0	0  | 0 ...			0	2	4	0	2	4  | 0   2   4 ...
   -1	0	1	2	0  | 0 ...				0	1	2	0	1  | 2   0   1 ...
	   -1	0	1	2  | 0 ...					0	0	0	0  | 0   0   0 ...
–––––––––––––----------|----						0  -1  -2  | 0  -1  -2 ...
-2 -1  -1	5	3	2  | 0 ...			———————————————————————|——————————
										0	2	5	2	1	3  | 2	 1	 3 ...
```

y[0] = -2	y[1] = -1	y[2] =5 ...			  y[0] = 0	y[1] = 2	y[2] = 5	y[3i] = 2	y[3i+1] = 1	y[3i+2] = 3	i >= 1

> ==Bei $\Delta t\ne 1\quad→\quad y=\Delta t·h\circledast x$==

<div style="page-break-after: always;"></div>

**Dividieren:	P ==O== lynom- / ==R== eihen-Division (niedrigste Potenz ==R==echts ==O==ben)**

$\quad x[n]=\dfrac{y[n]}{h[n]}\quad○—\!●\quad X(z)=\dfrac{4+2z^{-1}+2z^{-2}}{2-2z^{-1}+4z^{-2}}=\dfrac{2+z^{-1}+z^{-2}}{1-1z^{-1}+2z^{-^2}}$		Nenner: einzelne Zahl = 1

```
niedrige Po-		niedrige Potenzen hinten
tenzen oben
					| 2  1  1  0  0			← fehlende Potenz durch Null darstellen
'1' nicht		————|———————————————
notieren		 -2 |	   -4 -6  0 ..
				 +1 |	 2  3  0 -6 ..
umgekehrte		————|———————————————
Vorzeichen			| 2  3  0 -6 -6 ...
```



## 4.2 Stetige Faltung

$~\\\displaystyle\begin{eqnarray*}\bold{Faltung}\qquad\qquad\qquad&\bold{Korrelation}\\~\\x(t)\circledast h(t)=\int_{-\infty}^\infty x(\tau)\ h(t-\tau)\ \text{d}\tau&\qquad\qquad\phi_{xy}(t)=x(t)\star y(t)=\int_{-\infty}^\infty x(\tau)\ y(t+\tau)\ \text d\tau\\~\\&\qquad\phi_{xy}(t)=\lim_{T→\infty}\dfrac1{2T}\ \int_{-T}^T x(\tau)\ y(t+\tau)\ \text d\tau\end{eqnarray*}$





# 5. [Korrelation](https://youtu.be/MQm6ZP1F6ms?t=429)


$$
\phi_{xy}=x[n]\star y[n]\qquad=x[-n]\circledast y[n]\qquad=\sum_i x[i]·y[n+i]\\~\\\phi_{y}[n]\qquad○—\!●\qquad\phi_{xy}(z)=X(z^{-1})·Y(z)
$$

---

Autokorrelation:		$\phi_{xx}=x[n]\star x[n]$						Spiegelung:		$\phi_{yx}[n]=\phi_{xy}[-n]$



<u>**Position des Ausgangssignals**</u>

|                                      | Korrelation                            | Faltung                               |
| ------------------------------------ | -------------------------------------- | ------------------------------------- |
| horizontaler Bereich der Signale     | $x\ [n_0<n<n_1]\qquad h\ [n_2<n<n_3]$  | $x\ [n_1<n<n_0]\qquad h\ [n_2<n<n_3]$ |
| horizontaler Bereich des Ergebnisses | $y\ \big[(n_0+n_2)<n<(n_1+n_3){\big]}$ | $y\ \big[(n_1+n_2)<n<(n_0+n_3)\big]$  |



<div style="page-break-after: always;"></div>

# 6. Werkzeuge



## 6.1 Reelle Spektraldarstellung:

$u(t)=2a_0+(2\ a_1\cos(\omega_1 t)+2\ b_1\sin(\omega_1 t))+(2\ a_2\cos(\omega_2 t)+2\ b_2\sin(\omega_2 t))$

**System:**		<img src="../../Typora.assets/image-20200514081902019.png" alt="image-20200514081902019" style="zoom: 33%;" />		$\underline f(\omega)=\frac1{z_R+z_S}=\frac 1{1+j\omega L}$

| $\omega\ [s^{-1}]$ | kartesich [V] |      | kartesisch [$\frac1Ω$]                          |
| ------------------ | ------------- | ---- | ----------------------------------------------- |
| $0$                | $b_0$         |      | $\underline f(0)=\frac11$                       |
| $\omega_1$         | $b_1-c_1j$    |      | $\underline f(\omega_1)=\frac 1{1+j\omega_1 L}$ |
| $\omega_2$         | $b_2-c_2j$    |      | $\underline f(\omega_2)=\frac 1{1+j\omega_2 L}$ |

$i(t)=f(\omega)·u(t)\\=f(0)·2a_0+f(\omega_1)·\bigg[2\ a_1\cos(\omega_1 t)+2\ b_1\sin(\omega_1 t)\bigg]+f(\omega_2)\bigg[2\ a_2\cos(\omega_2 t)+2\ b_2\sin(\omega_2 t)\bigg]$



## 6.2 Netzwerkanalyse

| ==$R~\text{ immer gleich}$== | Normal | Zeiger                 | Fourrier             | Laplace       | Funktion            |
| ---------------------------- | ------ | ---------------------- | -------------------- | ------------- | ------------------- |
| **Spule**                    | $L$    | $j\omega L$            | $j\omega L$          | $sL$          | $u_L=L·\dot{(i_L)}$ |
| **Kondensator**              | $C$    | $\dfrac{-j}{\omega C}$ | $\dfrac1{j\omega C}$ | $\dfrac1{sC}$ | $i_C=C·\dot u_C$    |



## 6.3 [Maschenstromverfahren](https://www.youtube.com/watch?v=4YsF9SYlkQM)

<img src="./MA2%20Zusammenfassung/Maschenstromverfahren.png" alt="Maschenstromverfahren" style="zoom: 33%;" />

```
Wird durch-			i1			i2
flossen von:

1. Masche	  /	z1 +z2 +z3		-z2 -z3 \	  / i1 \	 / u \
			 |							 | x |	 	| = |	  |
2. Masche	  \	-z2 -z3		 z2 +z3 +z4 /	  \ i2 /	 \ 0 /
```
