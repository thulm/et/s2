# 2. Semester
## Inhalt
1. [ETGR2_Elektrotechnik](https://gitlab.com/thulm/et/s2/-/tree/main/ETGR2_Elektrotechnik)
2. [GKOMM_Grundlagen_der_Kommunikationstechnik](https://gitlab.com/thulm/et/s2/-/tree/main/GKOMM_Grundlagen_der_Kommunikationstechnik)
3. [MCOMP_Mikrocomputertechnik](https://gitlab.com/thulm/et/s2/-/tree/main/MCOMP_Mikrocomputertechnik)
4. [MATH2_Mathematik](https://gitlab.com/thulm/et/s2/-/tree/main/MATH2_Mathematik)
5. [PHYS2_Physik](https://gitlab.com/thulm/et/s2/-/tree/main/PHYS2_Physik)
6. [DIGT2_Digitaltechnik](https://gitlab.com/thulm/et/s2/-/tree/main/DIGT2_Digitaltechnik)

Für alle anderen Infos bitte die [README.md](https://gitlab.com/thulm/et/s1/-/blob/main/README.md) aus dem 1. Semester lesen!