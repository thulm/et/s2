### 2. Induktivität mit Luftspalt

| Name       | Elektrisch           | Magnetisch                                                   |
| ---------- | -------------------- | ------------------------------------------------------------ |
| Spannung   | $U=R*I=\dfrac IG$    | $\Theta=R_m*\Phi=\dfrac{\Phi}{\Lambda_m}\qquad\Theta=N*I$    |
| Spannung   | $U=E*d$              | $\Theta=H*l\qquad\Theta=H_{Fe}·l_{Fe}+H_L·l_L$               |
| Strom      | $I=\dfrac UR=G*U$    | $\Phi=\dfrac\Theta{R_m}=\Lambda_m*\Theta\qquad\Phi=B*A\qquad [\Phi]=Wb$ |
| Widerstand | $R=\dfrac UI$        | $R_m=\dfrac l{\mu A}=\dfrac{N^2}L\qquad [R_m]=\dfrac1H$      |
| Leitwert   | $G=\dfrac IU$        | $\Lambda_m=\dfrac {\mu A}l=\dfrac L{N^2} \qquad [\Lambda_m]=1H$ |
| Extra      | $[L]=1H=1\frac{Vs}A$ | $H=\dfrac B\mu\qquad \mu_r=\dfrac B{\mu_0H}\qquad \mu_0=4\pi·10^{-7}\frac Hm$ |

| Kreisrunder Querschnitt                                      |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20200504093112213.png" alt="image-20200504093112213" style="zoom: 50%;" /> | <img src="../../Typora.assets/image-20200504104343164.png" alt="image-20200504104343164" style="zoom: 25%;" /> |
| $\Theta=NI=\Phi(R_{mFe}+R_{ml})=\Phi(\frac{l_{Fe}}{\mu_0\mu_rA_{Fe}}+\frac d{\mu_0A_l})$ | $H_{Fe}(B_{Fe}=0)=\frac\Theta{l_{Fe}}\qquad B_{Fe}(H_{Fe}=0)=\Theta\frac{\mu_0A_l}{l_lA_{Fe}}$ |
| $L=N^2\frac{\mu_0\mu_rA}{l_{Fe}+\mu_rd}\underbrace\approx_{\mu_r \gg 1\\l_{Fe}\ll\mu_rd}N^2\frac{\mu_0A}d=N^2\Lambda_l$ | $\Theta=H_{Fe}(B_{Fe})l_{Fe}+H_ll_l$                         |
| $\mathcal A_L=\Lambda_m=\frac{\mu_0\mu_rA}{l_{Fe}+\mu_rd}$   | $B=I\sqrt{\frac LA\frac{\mu_0\mu_r}{l_{Fe}+\mu_rd}}$         |



### 3. Gegeninduktion und Übertrager

| Übertrager                                                   | Ersatzschaltbild                                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image.Q118L0](../../Typora.assets/image.Q118L0.png)        | ![image.H188L0](../../Typora.assets/image.H188L0.png)        |
| $k=\sqrt{k_{12}k_{21}}=\vert\frac M{\sqrt{L_{11}L_{22}}}\vert\qquad \sigma=1-k^2$ | $L_1=N_1^2\Lambda_m\qquad L_2=N_2^2\Lambda_m\qquad M=N_1N_2\Lambda_m$ |

| $u_1(t)=R_1i_i(t)+L_{11}{d\over dt}i_1(t)$                   | $u_1=R_1i_1+\frac d{dt}(\Psi_{11}+\Psi_{12})=R_1i_1+L_{11}\frac d{dt}i_1+L_{12}\frac d{dt}i_2$ |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $u_{2\ ind}(t)={d\Psi_{21}\over dt}=L_{21}\frac d{dt}i_1(t)$ | $u_1=R_2i_2+\frac d{dt}(\Psi_{21}+\Psi_{22})=R_2i_2+L_{21}\frac d{dt}i_1+L_{22}\frac d{dt}i_2$ |
|                                                              | (**Linker Index:** Wirkung,	**Rechter Index:** Ursache)   |

<img src="../../Typora.assets/image-20200507071820691.png" alt="image-20200507071820691" style="zoom: 45%;" />  <img src="../../Typora.assets/image-20200507072220747.png" alt="image-20200507072220747" style="zoom:67%;" />

​	<img src="../../Typora.assets/image-20200717154111765.png" alt="image-20200717154111765" style="zoom: 67%;" />							<img src="../../Typora.assets/image-20200507071924172.png" alt="image-20200507071924172" style="zoom: 67%;" />

| Gleichsinnige Eicklung                                       | Gegensinnige Wicklung                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20200507072404716](../../Typora.assets/image-20200507072404716.png) | ![image-20200507072420035](../../Typora.assets/image-20200507072420035.png) |



### 4. Lade- / Entladevorgang

|                                                              | **Induktivität**                                             | **Kapazität**                                                |
| ------------------------------------------------------------ | :----------------------------------------------------------- | ------------------------------------------------------------ |
| Formel                                                       | $L=\frac{N\Phi}I=N^2\frac\Phi\Theta=N^2\Lambda_m\\ [L]=1\frac{Vs}A$ | $C=\frac qU=\varepsilon_0\ \varepsilon_r\ \frac Ad$          |
| Strom                                                        | $i_L(t)=I(1-e^{-\frac{t-t_0}\tau})+I_ae^{-\frac{t-t_0}\tau}$ | $i_C(t)=\frac1R*(U-U_a)*e^{-\frac{t-t_0}\tau}$               |
| Spannung                                                     | $u_L(t)=\underbrace{R*(I-I_a)}_{u_L(t_0)}*e^{-\frac{t-t_0}\tau}$ | $u_C(t)=U(1-e^{-\frac{t-t_0}\tau})+U_ae^{-\frac{t-t_0}\tau}$ |
| Umgeschrieben                                                | $i_L(t)=I-(I-I_a)*e^{-\frac{t-t_0}\tau}$                     | $u_C(t)=U-(U-U_a)*e^{-\frac{t-t_0}\tau}$                     |
| (Ent-)Ladezeitkonstante (Innenwiderstand Ersatzquelle aus C/L-Sicht) | $\tau=\frac L{R_i}$                                          | $\tau=R_iC$                                                  |
| Anfangssteigung                                              | $\dot i_L(0)=\dfrac{I-I_a}\tau$                              | $\dot u_C(0)=\dfrac{U-U_a}\tau$                              |
| $\alpha=\dfrac1\tau$                                         | $k=(I-I_a)R$                                                 | $k=\frac{U-U_a}R$                                            |
| eingeschwungen (geladen)                                     | Kurzschluss                                                  | Leerlauf                                                     |
| entladen                                                     | Leerlauf                                                     | Kurzschluss                                                  |

| Quelle / Widerstand                                          | Ersatzspannungsquelle                                        | Ersatzstromquelle                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20200112152835816.png" alt="image-20200112152835816" style="zoom: 40%;" /> <img src="../../Typora.assets/image-20200112152915915.png" alt="image-20200112152915915" style="zoom:50%;" /> | <img src="../../Typora.assets/image-20200112192526267.png" alt="image-20200112192526267" style="zoom:50%;" />   <img src="../../Typora.assets/image-20200112171742500.png" alt="image-20200112171742500" style="zoom: 50%;" /> | <img src="../../Typora.assets/image-20200112192601937.png" alt="image-20200112192601937" style="zoom:50%;" />  <img src="../../Typora.assets/image-20200112171936204.png" alt="image-20200112171936204" style="zoom:50%;" /> |
| U + I entgegengesetzt<br />U + I gleich gerichtet            | Leerlaufspannung:  $U_{01}=U_2$<br />Quelle kurzschließen:  $R_{01}=R_1\parallel R_2$ | Kurzschlussstrom:  $I_{01}=I_1$<br />Quelle zu Leerlauf:  $R_{01}=R_1+R_2$ |

| $\tau=(R_N\parallel R_L)·C$                                  | $\tau=R_L·C$                                                 | $\tau=\dfrac L{R_N+R_L}$                                     | $\tau=\dfrac L{R_L}$                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20200726182152027](../../Typora.assets/image-20200726182152027.png) | ![image-20200726182200507](../../Typora.assets/image-20200726182200507.png) | ![image-20200726182216121](../../Typora.assets/image-20200726182216121.png) | ![image-20200726182227473](../../Typora.assets/image-20200726182227473.png) |



### 5. Kenngrößen und Messen

| Mittelwert x̄                                                 | Gleichrichtmittelwert                                        | Effektivwert                                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/Screenshot_20191107-114346.png" alt="Screenshot_20191107-114346" style="zoom: 8%;" /> | <img src="../../Typora.assets/Screenshot_20191107-114838.png" alt="Screenshot_20191107-114838" style="zoom:15%;" /> | <img src="../../Typora.assets/Screenshot_20191107-115706.png" alt="Screenshot_20191107-115706" style="zoom:15%;" /> |
| $\bar x=\frac 1T \int_{t_0}^{t_1}x(t)\ dt$                   | $\vert\bar x\vert=\frac 1T \int_{t_0}^{t_1}\vert x(t)\vert\ dt$ | $x_{eff}=\sqrt{\frac 1T \int_{t_0}^{t_1}(x(t))^2\ dt}$       |
|                                                              | $\vert\overline u_{sin}\vert=\hat u*\frac2\pi$               | $U_{eff}=\sqrt{U_{DC}^2+U_\omega^2}\qquad U_{sin}=\frac{\hat u}{\sqrt2}\qquad I_{sin}=\frac{\hat i}{\sqrt2}$ |
| Multimeter: DC                                               |                                                              | Multimeter: AC                                               |
| **Scheitelwert:** höchste Spannung                           | **Amplitude:** höchste Wechsel-Spannung                      | **Symmetrisch:** viermal U~eff~ einer viertel Periode        |
| **Formfaktor:**  $F_S=\dfrac{U_S}{\vert\overline {u_S}\vert}$ | $F_{sin}=\dfrac\pi{2\sqrt2}$                                 | $U_{eff}=\dfrac{F_S}{F_{sin}}\ U_{Anzeige}$                  |

<img src="../../Typora.assets/image-20200507073046744.png" alt="image-20200507073046744" style="zoom: 40%;" />			<img src="../../Typora.assets/image-20200707173714564.png" alt="image-20200707173714564" style="zoom:45%;" />

| Scheinwiderstand                                       | Wirkwiderstand (R)                  | Blindwiderstand (X~L~)                                   | Blindwiderstand (X~C~)                    |
| ------------------------------------------------------ | ----------------------------------- | -------------------------------------------------------- | ----------------------------------------- |
| $\dfrac{\underline u}{\underline i}=\underline Z=R+jX$ | $R$                                 | $X_L=2\pi f*L=\omega*L$                                  | $X_C=\dfrac1{2\pi f*C}=\dfrac1{\omega*C}$ |
| $\dfrac{\hat u}{\hat i}=\sqrt{\omega^2L^2+R^2}$        | $R=\dfrac{\omega L}{\tan(\varphi)}$ | $\dfrac{\hat u}{\hat i}=\dfrac{\omega L}{\sin(\varphi)}$ |                                           |



### 7. Komplex

| Reell                                                        | Komplex                                                      |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $u(t)=\hat u\ \cos(\omega t+\varphi)$                        | $\underline u(t)=\hat u\ e^{j(\omega t+\varphi)}=\hat u\ e^{j\varphi}e^{j\omega t}=\underline{\hat u}\ e^{j\omega t}\\\qquad=\hat u\ (\cos(\omega t+\varphi)+j\sin(\omega t+\varphi))$ |
| $u(t)=\hat u\ \sin(\omega t+\varphi)=\hat u\ \cos(\omega t+\varphi-90°)$ | $\underline u(t)=\hat u\ e^{j(\omega t+\varphi-90°)}=e^{-j90°}\hat u\ e^{j\varphi}e^{j\omega t}=-j\ \underline{\hat u}\ e^{j\omega t}\\\qquad=-j\ \hat u\ (\cos(\omega t+\varphi)+j\sin(\omega t+\varphi))$ |
| $i(t)=\hat i\ \cos(\omega t+\varphi)$                        | das selbe                                                    |
|                                                              | das selbe                                                    |
| Amplitude:  $\hat u=\vert \hat u\vert=\vert\underline Z\vert\ \hat i$ | <img src="../../Typora.assets/image-20200628135701384.png" alt="image-20200628135701384" style="zoom:67%;" /> |
| <img src="../../Typora.assets/image-20200628122749505.png" alt="image-20200628122749505" style="zoom: 50%;" />      $\hat u=U\sqrt2$ | $e^{j(\omega t+\varphi)}=\cos(\omega t+\varphi)+j\sin(\omega t+\varphi)$ |
| $\underline Z=\dfrac{\underline U}{\underline I}=\dfrac UI\ e^{j\varphi}=\dfrac UI*(\cos(\varphi)+j\sin(\varphi))$ | $\underline Z=\underbrace{\dfrac UI\cos(\varphi)}_{R}+j\ \underbrace{\dfrac UI\sin(\varphi)}_{X}$ |

| Scheinwiderstand<br />$\dfrac{\underline{\hat u}}{\underline{\hat i}}=\underline Z=R+jX$ | Wirkwiderstand  $R\\$<br />$[R]=\Omega$                      | Blindwiderstand  $X_L\\$<br />$[L]=\Omega\ s$                | Blindwiderstand  $X_C\\$<br />$[C]=\dfrac s\Omega$           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [**Zeigerdiagram**](https://www.youtube.com/watch?v=2HMqjaJZLrY) | <img src="../../Typora.assets/image.TKUTM0.png" alt="image.TKUTM0" style="zoom:33%;" /> | <img src="../../Typora.assets/image.EJVUM0.png" alt="image.EJVUM0" style="zoom:33%;" /> | <img src="../../Typora.assets/image.LXF2M0.png" alt="image.LXF2M0" style="zoom:20%;" /> |
| **W ==I== ederstand /<br />==I== mpedanz**                   | $\underline Z_R=R$                                           | $\underline Z_L=j\omega L\ (=jX_L)$                          | $\underline Z_C=\dfrac1{j\omega C}\ (=-jX_C)$                |
| **L ==A== itwert /<br />==A== dmittanz**                     | $\underline Y_R=\dfrac1R=G$                                  | $\underline Y_L=\dfrac1{j\omega L}\ (=-jB_L)$                | $\underline Y_C=j\omega C\ (=jB_C)$                          |

| Grenzkreisfrequenz                                          | Resonanzfrequenz                                             |
| ----------------------------------------------------------- | ------------------------------------------------------------ |
| $(\omega_0)\ /\ \omega_c=\dfrac1\tau=\dfrac RL=\dfrac1{RC}$ | $\omega_0=\dfrac1{\sqrt{LC}}\qquad \text{Im}\{\underline Z\}=0$ |

<div style="page-break-after: always;"></div>

### 8. [Leistung](https://youtu.be/FkRgFE86peI)


​	<img src="../../Typora.assets/image-20200707154154314.png" alt="image-20200707154154314" style="zoom:50%;" />			<img src="../../Typora.assets/image-20200707153855623.png" alt="image-20200707153855623" style="zoom:50%;" />

​																				Gleichanteil		Wechselanteil

p(t) > 0 → 2-Pol nimmt Leistung auf			p(t) < 0 → 2-Pol gibt Leistung ab

| Scheinleistung                                               | Wirkleistung  (R)                                            | Blindleistung  (X~L~ / X~C~)                                 |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $\underline S=\underline U*\underline I^*=P+jQ\\\ \\=\underline Z*\vert I\vert^2=\dfrac{U^2}{\underline Z}$ | Leistung, die durch Realteil zu Wärme wird                   | Leistung, die durch Imaginärteil in Spule (Q>0) oder Kondensator (Q<0) gespeichert wird |
| $S=\sqrt{P^2+Q^2}=UI=\dfrac12\ \hat u\hat i\\\ \\=\vert\underline Z\vert\ I^2=\dfrac{U^2}{\vert\underline Z\vert}$ | $P=\overline p=\dfrac12\ \hat u\hat i\cos(\varphi_u-\varphi_i)\\\ \\=UI\cos(\varphi_u-\varphi_i)=R*\vert\underline I\vert^2$ | $Q=\dfrac12\ \hat u\hat i\sin(\varphi_u-\varphi_i)\\\ \\=UI\sin(\varphi_u-\varphi_i)$ |
| $[S]=VA$                                                     | $[P]=W$                                                      | $[Q]=VAr$                                                    |

| Widerstand                                                   | Spule                                                        | Kondensator                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| $p_R(t)=\dfrac12\ \hat u\hat i\big[1\\+\cos(2\omega t+2\varphi_u)\big]$ | $p_L(t)=\dfrac12\ \hat u\hat i\sin(2\omega t+2\varphi_u)$    | $p_C(t)=-\dfrac12\ \hat u\hat i\sin(2\omega t+2\varphi_u)$   |
| $P_R=\overline{p_R}=\dfrac12\ \hat u\hat i=UI$               | $Q_L=\dfrac12\ \hat u\hat i=UI$                              | $Q_C=-\dfrac12\ \hat u\hat i=-UI$                            |
|                                                              | $W_m=\dfrac14\ L\hat i^2\big[1-\cos(2\omega t+2\varphi_u)\big]$ | $W_e=\dfrac14\ C\hat u^2\big[1+\cos(2\omega t+2\varphi_u)\big]$ |

Wirkfaktor  $\lambda=\cos(\varphi_u-\varphi_i)=\dfrac PS=\dfrac{\text{Re}(\underline S)}{\vert \underline S\vert}\qquad=\dfrac R{\vert Z\vert}=\dfrac R{\sqrt{R^2+X^2}}\qquad=\dfrac G{\vert Y\vert}=\dfrac G{\sqrt{G^2+B^2}}$

Blindstromkompensation:	$\text{Im}(\underline Z_V)=0$

| [Leistungsanpassung: Verbraucher Komplex](https://www.youtube.com/watch?v=zgZPil606tA) | Leistungsanpassung: Verbraucher rein Reell                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| $R_V=R_i\qquad \underline X_V=\underline X_i^*\qquad P_{V,max}=\dfrac{U^2}{4R_i}$ | $R_V=\sqrt{R_i^2+X_i^2}\quad P_{V,max}=\dfrac{U^2}{2\left(\sqrt{R_i^2+X_i^2}+R_i\right)}$ |



### 9. Netzwerkanalyse

| Impedanz:    $\underline Z=R+jX$                             | Admittanz:    $\underline Y=G+jB$                    |
| ------------------------------------------------------------ | ---------------------------------------------------- |
| $R=\dfrac G{G^2+B^2}\qquad X=\dfrac{-B}{G^2+B^2}$            | $G=\dfrac R{R^2+X^2}\qquad B=\dfrac{-X}{R^2+X^2}$    |
| $R_r=\dfrac{R·X^2}{R^2+X^2}\qquad X_r=\dfrac{R^2·X}{R^2+X^2}$ | $R_p=\dfrac{R^2+X^2}R\qquad X_p=-\dfrac{R^2+X^2}{X}$ |

Umformung gilt nur für eine Frequenz!

<img src="../../Typora.assets/image.W3GIN0.png" alt="image.W3GIN0" style="zoom:60%;" />

---

falls  $R_p=\omega_0L_p\qquad R_r=\dfrac{R_p}2\qquad L_r=\dfrac{L_p}2$

---

<img src="../../Typora.assets/image.QVBGN0.png" alt="image.QVBGN0" style="zoom:60%;" />

---

falls  $R_p=\dfrac1{\omega_0C_p}\qquad R_r=\dfrac{R_p}2\qquad C_r=2C_p$

---

| Spannungsquelle                                              | Stromquelle                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image.79QEN0.png" alt="image.79QEN0" style="zoom: 30%;" />             $\underline I=0\qquad\underline U_q=\dfrac{\underline I_q}{\underline Y_i}$ | <img src="../../Typora.assets/image.5548M0.png" alt="image.5548M0" style="zoom: 30%;" />              $\underline U=0\qquad\underline I_q=\dfrac{\underline U_q}{\underline Z_i}$ |
| <img src="../../Typora.assets/image-20200709150734704.png" alt="image-20200709150734704" style="zoom: 60%;" /> | <img src="../../Typora.assets/image-20200709150911034.png" alt="image-20200709150911034" style="zoom:60%;" /> |

<div style="page-break-after: always;"></div>

### 10. [Ortskurve](https://youtu.be/sQ1pOw5RvGE) und Bodediagramm

![image-20200709151731194](../../Typora.assets/image-20200709151731194.png)

| Resonanzfrequenz                                             | Normalform                                       |
| ------------------------------------------------------------ | ------------------------------------------------ |
| Z in Real- und Imaginärteil aufteilen                        | Z <u>nur</u> auf gemeinsamen Hauptnenner bringen |
| $\underline Z=\ \dfrac{\ ·\ }·+\ j\ \dfrac{\ ·\ }·$          | $\underline Z=\dfrac{..LC\omega+R..}·$           |
| $\text{Im}(\underline Z)\overset!=0\quad→\quad \omega_0=\sqrt{\dfrac{\ ·\ }·}$ |                                                  |
| $f_0=\dfrac{\omega_0}{2\pi}$                                 |                                                  |

| ——————<br />R + L | <img src="../../Typora.assets/image-20200709161454199.png" alt="image-20200709161454199" style="zoom:33%;" /> | ——————<br />R ∥ L | ![image-20200726174409610](../../Typora.assets/image-20200726174409610.png) |
| ----------------- | ------------------------------------------------------------ | ----------------- | ------------------------------------------------------------ |
| **R + C**         | ![image-20200726174252292](../../Typora.assets/image-20200726174252292.png) | **R ∥ C**         | <img src="../../Typora.assets/image-20200726175836546.png" alt="image-20200726175836546" style="zoom:50%;" /> |
| **R + L + C**     | ![image-20200709163931133](../../Typora.assets/image-20200709163931133.png) | **R ∥ L ∥ C**     | ![image-20200709165410964](../../Typora.assets/image-20200709165410964.png) |

| ———————(R + L) ∥ C  | <img src="../../Typora.assets/image.WHM1N0.png" alt="image.WHM1N0" style="zoom: 80%;" /> | <img src="../../Typora.assets/image.4SYUN0.png" alt="image.4SYUN0" style="zoom: 50%;" /> |
| ------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **R + (R ∥ C) + L** | <img src="../../Typora.assets/image.6BL9N0.png" alt="image.6BL9N0" style="zoom: 40%;" /> | <img src="../../Typora.assets/image.9KZ5N0.png" alt="image.9KZ5N0" style="zoom: 50%;" /> |

| R + C + L ∥ (R + C)<br />—————————<br />(R + L + C) ∥ (R + C) | ![Bildschirmfoto vom 2020-07-25 09-53-48](../../Typora.assets/Bildschirmfoto vom 2020-07-25 09-53-48.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **R ∥ C + (R + L) ∥ C<br />—————————<br />(R + L) ∥ (L + C) ∥ C** | ![image.C2GCO0](../../Typora.assets/image.C2GCO0.png)        |

![image-20200721162611490](../../Typora.assets/image-20200721162611490.png)

![image-20200722132100561](../../Typora.assets/image-20200722132100561.png)

![image-20200726104505603](../../Typora.assets/image-20200726104505603.png)

<div style="page-break-after: always;"></div>

#### 10.5 Resonanz

<img src="../../Typora.assets/image-20200721154622260.png" alt="image-20200721154622260" style="zoom: 40%;" />  <img src="../../Typora.assets/image-20200709152441175.png" alt="image-20200709152441175" style="zoom:40%;" />		**Güte:**	$Q_S=\dfrac B{f_0}$

| Bedämpfter Serienschwingkreis                                | Bedämpfter Parallelschwingkreis                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20200709162624129.png" alt="image-20200709162624129" style="zoom:40%;" /> | <img src="../../Typora.assets/image-20200709162645275.png" alt="image-20200709162645275" style="zoom:40%;" /> |
| $\underline Z_S=R+j\omega L-j\dfrac1{\omega C}$              | $\underline Y_S=G+j\omega C-j\dfrac1{\omega L}$              |
| $\omega_0=2\pi f_0=\dfrac1{\sqrt{LC}}$                       | $''$                                                         |
| $Q_S=\dfrac1R\sqrt{\dfrac LC}$                               | $Q_P=R\sqrt{\dfrac CL}$                                      |
| $\underline Z_S(\omega)=j\dfrac{\left(\frac\omega{\omega_0}\right)^2-1}{\omega C}$ | $\underline Z_P(\omega)=\dfrac{j\omega L}{1-\left(\frac\omega{\omega_0}\right)^2}$ |



### 11. Ersatzschaltbilder

| Draht-/Massewiderstand                                       | Spule                                                        | Kondensator                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20200717150025537](../../Typora.assets/image-20200717150025537.png) | ![image-20200717145648470](../../Typora.assets/image-20200717145648470.png) | ![image-20200717150853320](../../Typora.assets/image-20200717150853320.png) |
| $\omega_0\approx\sqrt{\dfrac{1-\frac{R^2C}{L}}{LC}}$         | $\omega_0\approx\sqrt{\dfrac1{L_pC_p}}$                      | $\omega_0\approx\sqrt{\dfrac1{LC}}$                          |

<div style="page-break-after: always;"></div>

### 12. [Übertrager](https://www.youtube.com/watch?v=F0jUYryRixo) / Transformator

$\boldsymbol{\underline U}=\boldsymbol{\underline Z\ \underline I}\qquad \begin{pmatrix}\underline U_1\\\underline U_2\end{pmatrix}=\begin{pmatrix}j\omega L_1&-j\omega M\\-j\omega M&j\omega L_2\end{pmatrix}\begin{pmatrix}\underline I_1\\\underline I_2\end{pmatrix}$

| Gleichsinnig                                                 | Gegensinnig                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="../../Typora.assets/image-20200717153759963.png" alt="image-20200717153759963" style="zoom: 50%;" /> | <img src="../../Typora.assets/image-20200717153814282.png" alt="image-20200717153814282" style="zoom: 50%;" /> |
| <img src="../../Typora.assets/image-20200717154220185.png" alt="image-20200717154220185" style="zoom: 60%;" /><br /><img src="../../Typora.assets/image-20200717154431647.png" alt="image-20200717154431647" style="zoom: 60%;" /><br /><img src="../../Typora.assets/image-20200717154500991.png" alt="image-20200717154500991" style="zoom: 60%;" /> | <img src="../../Typora.assets/image-20200722223204382.png" alt="image-20200722223204382" style="zoom: 50%;" />      $k=\sqrt{k_{12}k_{21}}=\vert\dfrac M{\sqrt{L_{11}L_{22}}}\vert\\\ \\\sigma=1-k^2$ |

| Verbraucher mit Parallelschaltung                            | Verbraucher mit Reihenschaltung                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![image-20200722204020697](../../Typora.assets/image-20200722204020697.png) | ![image-20200722204037979](../../Typora.assets/image-20200722204037979.png) |
| <img src="../../Typora.assets/image-20200722203838614.png" alt="image-20200722203838614" style="zoom: 50%;" /> | <img src="../../Typora.assets/image-20200722203857948.png" alt="image-20200722203857948" style="zoom:50%;" /> |



### Häufige Fehler

Kraft auf Leiterschleife wirkt auf jeweils **eine** Seite

Füllfaktor  $k=0.9$  beachten:	$A_{Fe}=a*b*0.9$

Admittanz / Impedanz:	anfangs immer als Doppelbruch angeben

Kennlinie Permeabilität:	besser  $\mu$  als  $\mu_r$  bestimmen

Resonanz:	Vor Nullsetzen des Imaginärteils immer auf gemeinsamen Hauptnenner bringen

Verbraucher liegt hinter Klemmen. Widerstände davor spielen keine Rolle

Normalform Übertrager:	$(1-k^2)$  ... vor den  $L_1$  und  $Ü^2$  vor den  $R$  stehen lassen und damit umformen