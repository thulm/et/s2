# Klausuraufgaben

> Diese Übersicht zeigt die Klausuraufgaben der letzten Jahre von Herrn Münzner
>
> Sie dient dazu zu sehen, welche Aufgabentypen in welchem Jahr und wie oft dran kamen
>
> In Klammern hinter den Überschriften steht das zugehörige Kapitel im Skript



### Spule (2)

| Jahr | Gegeben                                    | Gesucht                                                      |
| ---- | ------------------------------------------ | ------------------------------------------------------------ |
| 20   | Kern mit Luftspalt, l, A, d, N, µ~r~       | $L$,  → Änderung bei anderer Spulenposition                  |
| 19   | Spule mit Luftspalt, I, N, A, d, Kennlinie | $\mu_r=\frac 1{\mu_0}\frac BH$, Durchflutung, mag. Fluss, Induktivität |
| 18   | Spule mit Luftspalt, l, N, A, d, Kennlinie | Induktivität, max. Strom linearer Bereich                    |
| 17   | Spule o/m Luftspalt, l, A, d, L, Kennlinie | Windungszahl, Arbeitspunkt,                                  |
| 16   | Spule mit Luftspalt, l, N, A, d            | mag.-Kreis, A~L~, L,                                         |



### Einschwingvorgang (4)

| Jahr | Gegeben                | Gesucht                                                      |
| ---- | ---------------------- | ------------------------------------------------------------ |
| 20   | Stromkreis mit R, L, I | $u_R(t=0),\quad u_R(t\ge0s),\quad u_R(t=5\mu s)$             |
| 19   | Stromkreis mit R, L    | $u_L(t=0),\quad i_S(0\le t\le 10\mu s),\quad i_S(t=10\mu s)$,   Entladezeitkonstante + Zeit bis bestimmter Spannungsabfall |
| 18   | Stromkreis mit R, C    | $u_C(t=0),\quad u_C(t→∞),\quad u_C(t=0.5ms),\quad i_s(t=0.5ms)$ |
| 17   | Stromkreis mit R, C, I | $u_C(t=0),\quad \tau_L,\quad i_C(t\ge0s),\quad i_C(t=0s),\quad i_s(t=0.05ms)$,  Skizze i~C~(t) |
| 16   | Stromkreis mit R, L, U | $u_R/u_C(t=0),\quad \tau_E,\quad u_R(t\ge0), u(0,2ms)$  Ersatzschaltbild Entladevorgang, Skizze U (==umseitiges== Diagramm) |

<div style="page-break-after: always; break-after: page;"></div>

### Periodisches Signal (5?)

| Jahr | Gegeben            | Gesucht                                                      |
| ---- | ------------------ | ------------------------------------------------------------ |
| 20   | Signalverlauf U, I | $L, R$                                                       |
| 19   | Signalverlauf,     | Effektivwert, Gleichrichtwert Wechselanteil, Formfaktor sinusförmiges Signal (Umwandlung Rechteck → Sinus), Multimeter-Einstellungen |



### Komplexer Zweipol (7)

| Jahr | Gegeben               | Gesucht                                                      |
| ---- | --------------------- | ------------------------------------------------------------ |
| 20   | C + R + (L∥R) ,  f~0~ | $C$  so dass  $f_0$,  $\underline Z(f_0)$                    |
| 19   |                       | Impedanz, Impedanz in Normalform, Resonanzfrequenz, Grenzfrequenzen,<br />Skizze Frequenzgang Scheinwiderstand |
| 18   | R, L, C               | Admittanz, Resonanzfrequenz, Addmitanz bei Resonanzfrequenz  |
| 17   | R, L, C               | Resonanzfrequenz, Impedanz bei Resonanzfr., Güte, Ortskurve Z, |



### [Zeigerdiagramm](https://www.youtube.com/watch?v=2HMqjaJZLrY&t=111s) (7)

| Jahr | Gegeben                          | Gesucht                                                      |
| ---- | -------------------------------- | ------------------------------------------------------------ |
| 20   | Zweipol<br />(L∥R) + ((R+L)∥R∥C) | qualitatives Zeigerdiagramm aller I + U                      |
| 19   | Zweipol R, L, C                  | Zeiger-Skizze aller Impedanzen (Admittanzen)                 |
| 18   | Zweipol R, L, C                  | Zeiger-Skizze aller Ströme + Spannungen                      |
| 17   | Zweipol R, L                     | Zeiger aller I+U, Wirkfaktor, Bauteil für Blindstromkompensation |
| 16   | Zweipol R, L, I                  | Zeiger aller I+U                                             |

<div style="page-break-after: always; break-after: page;"></div>

### Übertrager / Transformator / Leistung (8)

| Jahr | Gegeben                            | Gesucht                                                      |
| ---- | ---------------------------------- | ------------------------------------------------------------ |
| 20   | Transformator mit U, f, L, R~L~, k | Ersatzschaltbild, Wiekleistund, Blindleistung, Wirkfaktor    |
| 19   | Übertrager mit U, f, L, σ          | Ersatzschaltbild, R + C bei max. Leistung, Wirkleistung, Blindleistung |
| 18   | Übertrager mit R, L, k, Z~i~       | Wirkfaktor,  $\underline Z_i$  bei max. Leistung             |
| 17   | Übertrager mit L, k, f             | Eingangsimpedanz, $\underline Z_i(\omega)$ Normalform, Grenzfrequenzen |
| 16   | Übertrager L, R, U, f              | L1, L2, M, k, σ, Eingangsadmittanz, Scheinleistung, Wirkfaktor |



### Bode-Diagramm (10+11)

| Jahr | Gegeben | Gesucht                                               |
| ---- | ------- | ----------------------------------------------------- |
| 20   | Bode-D  | Ersatzschaltbild für 2-Pol aus R, L, C,  Bauteilwerte |
| 17   | Bode-D  | Ersatzschaltbild für 2-Pol                            |
| 16   | Bode-D  | Ersatzschaltbild 2-Pol                                |



### ~~Filter (12+13)~~ 	kam im SS20 nicht dran

| Jahr | Gegeben             | Gesucht                                                      |
| ---- | ------------------- | ------------------------------------------------------------ |
| 18   | Vierpol mit R, L, C | Filtertyp,  $\underline H_u$ , Grenzfrequenz, Einfügedämpfung, Skizze Frequenzgang |
| 16   | Vierpol R, L, C     | Y-Matrix, $\underline H_u$ (==Normalform==), Bode-Diagramm skizzieren |
|      |                     |                                                              |





