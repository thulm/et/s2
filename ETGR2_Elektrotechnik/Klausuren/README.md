# Offizielle Klausuren
Unter den folgenden Links findest du veröffentlichte Altklausuren:
- Prof. Boeker: [THU Laufwerk](https://fs.hs-ulm.de/public/boeker/ET2/Übung/Probe_Kl_ET2_SS13/)
- Prof. Gölz: [THU Laufwerk](https://fs.hs-ulm.de/public/jgoelz/ETGR2/Probeklausur_Böker/)
- Prof. Münzner: [THU Laufwerk](https://fs.hs-ulm.de/public/muenzner/Vorlesungen/ETGR/Klausuren/)