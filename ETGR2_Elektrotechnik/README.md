# ETGR2_Elektrotechnik
Bitte Regeln im [README.md](https://gitlab.com/thulm/et/s1/-/blob/main/README_ANKI.md) vom 1. Semester Beachten.

### Erfahrungen/Tipps Georg Missel SS21
#### Vorlesung
- Skript und Vorlesung war anspruchsvoll, viele Herleitungen und viel Theorie. Man muss selbst merken was wichtig ist und was überflüssig ist. Labor: gut machbar, auch verständnisvoll.
#### Klausur
- dementsprechend schwere Klausur, aber die Bewertung ist sehr gut!!