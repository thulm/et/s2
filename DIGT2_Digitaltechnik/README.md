# LSFKurzbezeichnung(NR)_Name
Bitte Regeln im [README.md](https://gitlab.com/thulm/et/s1/-/blob/main/README_ANKI.md) vom 1. Semester Beachten.

### Erfahrungen/Tipps @ThorbenCarl WS20
#### Vorlesung
- Gute Vorlesung, Strukturierte Unterlagen
#### Klausur
- Klausur anspruchsvoll aber machbar

### Erfahrungen/Tipps David Sievers WS20
#### Vorlesung
- Fragen gut beantwortet; einige Übungsaufgaben vorhanden, welche aber nicht alles für die Klausur abdecken; sehr gutes Skript mit PowerPoint; gute Vorlesungen; sehr gut Organisiert
#### Klausur
- Klausur machbar, wenn auch bei VHDL mit Aufwand

### Erfahrungen/Tipps Georg Missel SS21
#### Vorlesung
- Siehe Aufschriebe der anderen. Bei den Laboren Wird Vorwissen aus DT1 genutzt (KVDs und WHTs). Anderer Syntax als C, ist etwas gewöhnungsbedürftig.
#### Klausur
- 

### Erfahrungen/Tipps  @DanielGlaeser WS21
#### Vorlesung
- Sehr interessante Vorlesung. VHDL und Halbleiterspeicher. Schön, verständlich und kompetent bezüglich Fragen zum Stoff oder im Labor vorgetragen. Lesefreundliches Skript, „gemeinsames durcharbeiten“ des eigenen Skripts vom Professor. Spannendes Labor. 4 zusammenhängende Einzeltermine entsprechend dem Wissensstand aus der Vorlesung.
#### Klausur
- 